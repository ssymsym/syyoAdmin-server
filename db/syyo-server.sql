/*
 Navicat Premium Data Transfer

 Source Server         : centos7-docker2(5.128)
 Source Server Type    : MySQL
 Source Server Version : 50647
 Source Host           : 192.168.5.128:3306
 Source Schema         : syyo-server

 Target Server Type    : MySQL
 Target Server Version : 50647
 File Encoding         : 65001

 Date: 28/09/2020 14:23:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_config
-- ----------------------------
DROP TABLE IF EXISTS `gen_config`;
CREATE TABLE `gen_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表名',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作者',
  `cover` bit(1) DEFAULT NULL COMMENT '是否覆盖',
  `module_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '模块名称',
  `pack` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '包名',
  `api_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '前端路劲',
  `api_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接口名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成配置类' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_config
-- ----------------------------
INSERT INTO `gen_config` VALUES (1, 'sys_user', 'wang', b'1', 'syyo-generator', 'com.syyo', 'C:\\Users\\Administrator\\Desktop\\beiyong\\gen', '用户');
INSERT INTO `gen_config` VALUES (4, 'log', 'wang', b'0', 'syyo-logging', 'com.syyo', 'C:\\Users\\Administrator\\Desktop\\beiyong\\gen', '日志');

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作人',
  `log_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作状态',
  `error_msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '错误信息',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '方法',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '操作内容',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ip',
  `create_time` datetime(0) DEFAULT NULL COMMENT '操作日期',
  `elapsed_time` bigint(20) DEFAULT NULL COMMENT '耗时',
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '浏览器',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 206 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES (11, '添加部门', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysDeptController.add()', '{\"deptName\":\"SYYO公司1\",\"pidList\":[]}', '0:0:0:0:0:0:0:1', '2020-09-02 18:22:59', 129, 'Unknown');
INSERT INTO `log` VALUES (12, '部门编辑', 'admin', 'error', '菜单修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', '{\"deptId\":2,\"deptName\":\"SYYO公司111\",\"pidList\":[1]}', '0:0:0:0:0:0:0:1', '2020-09-02 18:24:19', 72, 'Unknown');
INSERT INTO `log` VALUES (13, '部门编辑', 'admin', 'error', '菜单修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', '{\"deptId\":2,\"deptName\":\"SYYO公司111\",\"pidList\":[1]}', '0:0:0:0:0:0:0:1', '2020-09-02 18:31:08', 70, 'Unknown');
INSERT INTO `log` VALUES (14, '部门新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysDeptController.add()', '{\"deptName\":\"SYYO公司1\",\"pidList\":[]}', '0:0:0:0:0:0:0:1', '2020-09-02 18:31:11', 7, 'Unknown');
INSERT INTO `log` VALUES (15, '部门新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysDeptController.add()', '{\"deptName\":\"SYYO公司1\",\"pidList\":[]}', '0:0:0:0:0:0:0:1', '2020-09-02 18:38:13', 149, 'Unknown');
INSERT INTO `log` VALUES (16, '部门新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysDeptController.add()', '{\"deptName\":\"SYYO公司1\",\"pidList\":[]}', '0:0:0:0:0:0:0:1', '2020-09-02 18:39:23', 158, 'Unknown');
INSERT INTO `log` VALUES (17, '部门新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysDeptController.add()', '{\"deptName\":\"SYYO公司1\",\"pidList\":[]}', '0:0:0:0:0:0:0:1', '2020-09-02 18:40:24', 5, 'Unknown');
INSERT INTO `log` VALUES (18, '部门编辑', 'wang2', 'error', '菜单修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', '{\"deptId\":13,\"deptName\":\"SYYO公司1\"}', '172.20.64.1', '2020-09-03 14:47:37', 3, 'Firefox 7');
INSERT INTO `log` VALUES (19, '部门新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysDeptController.add()', '{\"deptName\":\"部门1\",\"pidList\":[]}', '0:0:0:0:0:0:0:1', '2020-09-05 17:08:37', 42, 'Chrome 8');
INSERT INTO `log` VALUES (20, '部门编辑', 'admin', 'error', '菜单修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=9, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-05 17:55:57', 3, 'Chrome 8');
INSERT INTO `log` VALUES (21, '部门编辑', 'admin', 'error', '菜单修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=9, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-05 17:56:57', 1, 'Chrome 8');
INSERT INTO `log` VALUES (22, '用户新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysUserController.add()', 'UserReq(userId=null, userName=wang3, nickName=, passWord=123456, newPassWord=null, token=null, phone=null, email=212, sex=1, avatar=null, remark=, status=null, roleIdList=[1])', '0:0:0:0:0:0:0:1', '2020-09-05 18:09:59', 113, 'Chrome 8');
INSERT INTO `log` VALUES (23, '用户编辑', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysUserController.edit()', 'UserReq(userId=4, userName=wang3, nickName=123, passWord=null, newPassWord=null, token=null, phone=null, email=212, sex=1, avatar=null, remark=null, status=1, roleIdList=[1])', '0:0:0:0:0:0:0:1', '2020-09-05 18:10:15', 29, 'Chrome 8');
INSERT INTO `log` VALUES (24, '用户删除', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysUserController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-05 18:10:50', 18, 'Chrome 8');
INSERT INTO `log` VALUES (25, '角色新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.add()', 'RoleReq(roleId=null, roleName=ceshi1, roleKey=cesh, remark=, menuIdList=null)', '0:0:0:0:0:0:0:1', '2020-09-05 18:11:18', 16, 'Chrome 8');
INSERT INTO `log` VALUES (26, '角色编辑', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.edit()', 'RoleReq(roleId=3, roleName=ceshi1, roleKey=cesh, remark=1, menuIdList=[])', '0:0:0:0:0:0:0:1', '2020-09-05 18:11:24', 3, 'Chrome 8');
INSERT INTO `log` VALUES (27, '角色分配权限', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.auth()', 'RoleReq(roleId=3, roleName=null, roleKey=null, remark=null, menuIdList=[3, 4])', '172.20.64.1', '2020-09-05 18:12:32', 14, 'Chrome 8');
INSERT INTO `log` VALUES (28, '角色删除', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.del()', '3', '172.20.64.1', '2020-09-05 18:12:51', 4, 'Chrome 8');
INSERT INTO `log` VALUES (29, '菜单新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysMenuController.add()', 'MenuReq(menuId=null, menuName=测试1, pid=null, pids=null, perms=, path=, component=, componentName=, hidden=false, cache=false, icon=alipay, type=1, sort=1, pidList=[])', '172.20.64.1', '2020-09-05 18:13:22', 6, 'Chrome 8');
INSERT INTO `log` VALUES (30, '菜单编辑', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=60, menuName=测试12, pid=0, pids=null, perms=null, path=null, component=null, componentName=null, hidden=false, cache=false, icon=alipay, type=1, sort=1, pidList=[0])', '172.20.64.1', '2020-09-05 18:13:29', 7, 'Chrome 8');
INSERT INTO `log` VALUES (31, '菜单删除', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysMenuController.del()', '60', '172.20.64.1', '2020-09-05 18:13:34', 5, 'Chrome 8');
INSERT INTO `log` VALUES (32, '部门新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=测试1, pidList=[])', '172.20.64.1', '2020-09-05 18:13:54', 4, 'Chrome 8');
INSERT INTO `log` VALUES (33, '部门编辑', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=19, deptName=测试12, pidList=null)', '172.20.64.1', '2020-09-05 18:13:58', 4, 'Chrome 8');
INSERT INTO `log` VALUES (34, '用户修改密码', 'admin', 'error', '旧密码错误', 'com.syyo.admin.controller.SysUserController.editPwd()', 'UserReq(userId=null, userName=null, nickName=null, passWord=HobM92n+XtmS3Lgzi2CgwwK4DJELUTiiFABeHtxy2v2JigOHoOvxojjhPKtZ+InoClaC0VpAzbGrcZVUA1p9o8sI6cLpkPZt9I0Sb877uzQgn6GKhBWhsiF6TikceqtbtlD8zkfymsPV9Yc5A/5fTEx/kOthW23U6i3mBt9aKvw=, newPassWord=DD+GD7DjiSDql3ATT1hvYGJRQWsDkp/P17W3DVrgvw2gsxBAYJmz/At50dPKCD2RrctJcI0/3J4YfsfTX2dxSyjmL4hLppa1htbn0KO+NPaySPUFw2taJuASaqAw0bsntQiEewD37Oj+dv+ByY0WpNdyBdRXZvN/sSD6YHr0zu8=, token=null, phone=null, email=null, sex=null, avatar=null, remark=null, status=null, roleIdList=null)', '0:0:0:0:0:0:0:1', '2020-09-07 11:20:26', 81, 'Chrome 8');
INSERT INTO `log` VALUES (35, '用户修改密码', 'admin', 'error', '旧密码错误', 'com.syyo.admin.controller.SysUserController.editPwd()', 'UserReq(userId=null, userName=null, nickName=null, passWord=sIxiWFS2r0iUzqdilUj09lpAq6YSiwlEBS7Ebd4igECt7D4QW+GsGj+0hnKfWPQ6o6egyWdV8ZX8iXi9lGJ/FVeVnF+lIMIwVFh8AMhCykpPRkWjT8dmI3qP7+EY45AHugVRRSicJpjHe9KDp8F0wjOHlg7dGEZ1yS3tvOcuzg4=, newPassWord=ehdg3yAC3LMF2FWvhZfgi7/3zL2lGkt/uTkUSNIMqS85n2wUDjTEpXjTQ+IZfgyWxaxz7snXKVzcZ+IbrMR2hVn/yfoMPM+kcLfOZPS2wJhhrp65yPR4T5DqhBclNGAkkpagX5l3XSjCHyaQ4zKgWRiRuMtKNxWvcuPR1oI+/J4=, token=null, phone=null, email=null, sex=null, avatar=null, remark=null, status=null, roleIdList=null)', '0:0:0:0:0:0:0:1', '2020-09-07 11:45:15', 72, 'Chrome 8');
INSERT INTO `log` VALUES (36, '用户修改密码', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysUserController.editPwd()', 'UserReq(userId=null, userName=null, nickName=null, passWord=i02ffnO3kT2bro14UdLwVMIAYOavOZokOLcW2bpKOJE+wpR7a3sKhR2JTdSio79CIiJ1RzYV6SeNPGxTWeS7EQ==, newPassWord=XlM9lgcj28wZf7VGXAhQmSMQxvUy7DfucBt7uL7yFrY6v9m39KW/cXlWrRJ2qaL67fcNBJCe83mVQQ10ncSCCg==, token=null, phone=null, email=null, sex=null, avatar=null, remark=null, status=null, roleIdList=null)', '0:0:0:0:0:0:0:1', '2020-09-07 12:04:49', 241, 'Chrome 8');
INSERT INTO `log` VALUES (37, '用户修改密码', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysUserController.editPwd()', 'UserReq(userId=null, userName=null, nickName=null, passWord=jDYYY/hkssqT4Clah8ioTE4zgCA6DpNpBXk1wOehAOW2lfU/C7ekkZryA+oPmF7Sf3T8kloZfkDLjAPSrqKCeQ==, newPassWord=MU2VDytxYAy+fdRKF6foVQvWN4TV+RxAyQPYjC6eL9vBqG2Yh50xz76PtnkmieF59EfK73cpArLaM+tinW8GTg==, token=null, phone=null, email=null, sex=null, avatar=null, remark=null, status=null, roleIdList=null)', '0:0:0:0:0:0:0:1', '2020-09-07 12:05:25', 206, 'Chrome 8');
INSERT INTO `log` VALUES (38, '角色新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.add()', 'RoleReq(roleId=null, roleName=juese1, roleKey=key1, remark=, menuIdList=null)', '0:0:0:0:0:0:0:1', '2020-09-09 10:12:53', 7397, 'Chrome 8');
INSERT INTO `log` VALUES (39, '角色新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.add()', 'RoleReq(roleId=null, roleName=ceshi, roleKey=ceshi, remark=, menuIdList=null)', '192.168.5.49', '2020-09-09 10:14:57', 5, 'Chrome 8');
INSERT INTO `log` VALUES (40, '角色新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.add()', 'RoleReq(roleId=null, roleName=ceshi, roleKey=ceshi, remark=, menuIdList=null)', '192.168.5.49', '2020-09-09 10:14:57', 13, 'Chrome 8');
INSERT INTO `log` VALUES (41, '角色删除', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.del()', '6', '192.168.5.49', '2020-09-09 10:15:07', 3, 'Chrome 8');
INSERT INTO `log` VALUES (42, '角色删除', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.del()', '5', '192.168.5.49', '2020-09-09 10:15:14', 4, 'Chrome 8');
INSERT INTO `log` VALUES (43, '角色新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.add()', 'RoleReq(roleId=null, roleName=测试1, roleKey=ceshi, remark=, menuIdList=null)', '192.168.5.49', '2020-09-09 10:15:28', 3, 'Chrome 8');
INSERT INTO `log` VALUES (44, '角色新增', 'admin', 'info', NULL, 'com.syyo.admin.controller.SysRoleController.add()', 'RoleReq(roleId=null, roleName=测试2, roleKey=ceshi, remark=, menuIdList=null)', '192.168.5.49', '2020-09-09 10:17:34', 4, 'Chrome 8');
INSERT INTO `log` VALUES (45, '菜单新增', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.add()', 'MenuReq(menuId=null, menuName=多级菜单, pid=null, pids=null, perms=, path=nested, component=, componentName=, hidden=false, cache=false, icon=menu, type=1, sort=70, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-14 16:26:49', 16, 'Chrome 8');
INSERT INTO `log` VALUES (46, '菜单新增', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.add()', 'MenuReq(menuId=null, menuName=二级菜单1, pid=null, pids=null, perms=, path=menu1, component=, componentName=nested/menu1/index, hidden=false, cache=false, icon=menu, type=2, sort=71, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 16:27:59', 8, 'Chrome 8');
INSERT INTO `log` VALUES (47, '菜单新增', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.add()', 'MenuReq(menuId=null, menuName=二级菜单2, pid=null, pids=null, perms=, path=menu2, component=, componentName=nested/menu2/index, hidden=false, cache=false, icon=menu, type=2, sort=72, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 16:29:01', 4, 'Chrome 8');
INSERT INTO `log` VALUES (48, '菜单新增', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.add()', 'MenuReq(menuId=null, menuName=三级菜单1, pid=null, pids=null, perms=, path=menu1-1, component=nested/menu1/menu1-1, componentName=, hidden=false, cache=false, icon=menu, type=2, sort=711, pidList=[61, 62])', '0:0:0:0:0:0:0:1', '2020-09-14 16:29:52', 6, 'Chrome 8');
INSERT INTO `log` VALUES (49, '菜单新增', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.add()', 'MenuReq(menuId=null, menuName=三级菜单2, pid=null, pids=null, perms=, path=menu1-2, component=nested/menu1/menu1-2, componentName=, hidden=false, cache=false, icon=menu, type=2, sort=1, pidList=[61, 62])', '0:0:0:0:0:0:0:1', '2020-09-14 16:30:26', 4, 'Chrome 8');
INSERT INTO `log` VALUES (50, '角色分配权限', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysRoleController.auth()', 'RoleReq(roleId=1, roleName=null, roleKey=null, remark=null, menuIdList=[1, 2, 3, 4, 5, 6, 35, 36, 7, 8, 9, 10, 11, 34, 12, 13, 14, 15, 16, 37, 38, 39, 40, 41, 17, 21, 51, 52, 53, 33, 54, 55, 56, 57, 59, 61, 62, 65, 64, 63])', '0:0:0:0:0:0:0:1', '2020-09-14 16:30:40', 9, 'Chrome 8');
INSERT INTO `log` VALUES (51, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=62, menuName=二级菜单1, pid=61, pids=null, perms=null, path=menu1, component=nested/menu1/index, componentName=, hidden=false, cache=false, icon=menu, type=2, sort=71, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 16:57:50', 18, 'Chrome 8');
INSERT INTO `log` VALUES (52, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=62, menuName=二级菜单1, pid=61, pids=null, perms=null, path=menu1, component=nested/menu1/index, componentName=, hidden=false, cache=false, icon=menu, type=2, sort=71, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 16:58:01', 56, 'Chrome 8');
INSERT INTO `log` VALUES (53, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=62, menuName=二级菜单1, pid=61, pids=null, perms=null, path=menu1, component=nested/menu1/index, componentName=, hidden=false, cache=false, icon=menu, type=2, sort=71, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 16:58:10', 5, 'Chrome 8');
INSERT INTO `log` VALUES (54, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=63, menuName=二级菜单2, pid=61, pids=null, perms=null, path=menu2, component=nested/menu2/index, componentName=, hidden=false, cache=false, icon=menu, type=2, sort=72, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 16:58:31', 4, 'Chrome 8');
INSERT INTO `log` VALUES (55, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=63, menuName=二级菜单2, pid=61, pids=null, perms=null, path=menu2, component=nested/menu2/index, componentName=nested/menu2/index, hidden=false, cache=false, icon=menu, type=2, sort=72, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 17:00:03', 22245, 'Chrome 8');
INSERT INTO `log` VALUES (56, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=62, menuName=二级菜单1, pid=61, pids=null, perms=null, path=menu1, component=nested/menu1/index, componentName=, hidden=false, cache=false, icon=menu, type=2, sort=71, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 17:00:28', 13097, 'Chrome 8');
INSERT INTO `log` VALUES (57, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=62, menuName=二级菜单1, pid=61, pids=null, perms=null, path=menu1, component=nested/menu1/index, componentName=Menu1, hidden=false, cache=false, icon=menu, type=2, sort=71, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 17:00:58', 47, 'Chrome 8');
INSERT INTO `log` VALUES (58, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=63, menuName=二级菜单2, pid=61, pids=null, perms=null, path=menu2, component=nested/menu2/index, componentName=Menu2, hidden=false, cache=false, icon=menu, type=2, sort=72, pidList=[61])', '0:0:0:0:0:0:0:1', '2020-09-14 17:01:06', 4, 'Chrome 8');
INSERT INTO `log` VALUES (59, '菜单编辑', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysMenuController.edit()', 'MenuReq(menuId=65, menuName=三级菜单2, pid=62, pids=null, perms=null, path=menu1-2, component=nested/menu1/menu1-2, componentName=null, hidden=false, cache=false, icon=menu, type=2, sort=712, pidList=[61, 62])', '0:0:0:0:0:0:0:1', '2020-09-14 17:10:35', 16, 'Chrome 8');
INSERT INTO `log` VALUES (60, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=打算啊, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:42:06', 7, 'Chrome 8');
INSERT INTO `log` VALUES (61, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=打算啊, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:42:15', 1, 'Chrome 8');
INSERT INTO `log` VALUES (62, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:45:08', 1, 'Chrome 8');
INSERT INTO `log` VALUES (63, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大声道, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:49:42', 2, 'Chrome 8');
INSERT INTO `log` VALUES (64, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=达到的, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:52:26', 4, 'Chrome 8');
INSERT INTO `log` VALUES (65, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:54:25', 2, 'Chrome 8');
INSERT INTO `log` VALUES (66, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:54:33', 0, 'Chrome 8');
INSERT INTO `log` VALUES (67, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=的撒打算, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:54:50', 1, 'Chrome 8');
INSERT INTO `log` VALUES (68, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:55:04', 1, 'Chrome 8');
INSERT INTO `log` VALUES (69, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:55:21', 2, 'Chrome 8');
INSERT INTO `log` VALUES (70, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:55:52', 0, 'Chrome 8');
INSERT INTO `log` VALUES (71, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:56:21', 2, 'Chrome 8');
INSERT INTO `log` VALUES (72, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=达到ad, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:56:37', 1, 'Chrome 8');
INSERT INTO `log` VALUES (73, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=的撒打算, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:56:55', 2, 'Chrome 8');
INSERT INTO `log` VALUES (74, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=发的说法是, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:57:23', 1, 'Chrome 8');
INSERT INTO `log` VALUES (75, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 14:57:40', 1, 'Chrome 8');
INSERT INTO `log` VALUES (76, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:01:05', 1, 'Chrome 8');
INSERT INTO `log` VALUES (77, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:02:52', 2, 'Chrome 8');
INSERT INTO `log` VALUES (78, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:02:55', 2, 'Chrome 8');
INSERT INTO `log` VALUES (79, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=的撒打算, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:07:53', 1, 'Chrome 8');
INSERT INTO `log` VALUES (80, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大萨达, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:08:15', 2, 'Chrome 8');
INSERT INTO `log` VALUES (81, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:08:26', 0, 'Chrome 8');
INSERT INTO `log` VALUES (82, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大声道撒, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:09:07', 1, 'Chrome 8');
INSERT INTO `log` VALUES (83, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大声大声道, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:09:25', 2, 'Chrome 8');
INSERT INTO `log` VALUES (84, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=的撒打算, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:12:18', 1, 'Chrome 8');
INSERT INTO `log` VALUES (85, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=的撒打算, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:12:42', 1, 'Chrome 8');
INSERT INTO `log` VALUES (86, '部门新增', 'admin', 'error', '重新登录！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=的撒打算, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:13:47', 1, 'Chrome 8');
INSERT INTO `log` VALUES (87, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:40:30', 8, 'Chrome 8');
INSERT INTO `log` VALUES (88, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:42:03', 1, 'Chrome 8');
INSERT INTO `log` VALUES (89, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:42:58', 1, 'Chrome 8');
INSERT INTO `log` VALUES (90, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:43:42', 1, 'Chrome 8');
INSERT INTO `log` VALUES (91, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:44:19', 1, 'Chrome 8');
INSERT INTO `log` VALUES (92, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:46:03', 1, 'Chrome 8');
INSERT INTO `log` VALUES (93, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:46:18', 0, 'Chrome 8');
INSERT INTO `log` VALUES (94, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:47:34', 1, 'Chrome 8');
INSERT INTO `log` VALUES (95, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:47:36', 2, 'Chrome 8');
INSERT INTO `log` VALUES (96, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:48:01', 1, 'Chrome 8');
INSERT INTO `log` VALUES (97, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:50:08', 2, 'Chrome 8');
INSERT INTO `log` VALUES (98, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:50:24', 1, 'Chrome 8');
INSERT INTO `log` VALUES (99, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:51:02', 1, 'Chrome 8');
INSERT INTO `log` VALUES (100, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:53:24', 1, 'Chrome 8');
INSERT INTO `log` VALUES (101, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:53:43', 1, 'Chrome 8');
INSERT INTO `log` VALUES (102, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:54:17', 1, 'Chrome 8');
INSERT INTO `log` VALUES (103, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=的撒打算, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:55:12', 2, 'Chrome 8');
INSERT INTO `log` VALUES (104, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:58:22', 1, 'Chrome 8');
INSERT INTO `log` VALUES (105, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨发, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 15:58:51', 2, 'Chrome 8');
INSERT INTO `log` VALUES (106, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大声大声道, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:00:10', 1, 'Chrome 8');
INSERT INTO `log` VALUES (107, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:00:50', 1, 'Chrome 8');
INSERT INTO `log` VALUES (108, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=规范化规范化规范化, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:01:53', 1, 'Chrome 8');
INSERT INTO `log` VALUES (109, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=和规范化股份和, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:02:07', 1, 'Chrome 8');
INSERT INTO `log` VALUES (110, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=鬼地方个地方官的费, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:02:28', 1, 'Chrome 8');
INSERT INTO `log` VALUES (111, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨发, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:05:13', 0, 'Chrome 8');
INSERT INTO `log` VALUES (112, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:06:28', 226, 'Chrome 8');
INSERT INTO `log` VALUES (113, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:06:38', 1, 'Chrome 8');
INSERT INTO `log` VALUES (114, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:07:18', 2, 'Chrome 8');
INSERT INTO `log` VALUES (115, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:07:36', 2, 'Chrome 8');
INSERT INTO `log` VALUES (116, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:07:54', 1, 'Chrome 8');
INSERT INTO `log` VALUES (117, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:08:36', 1, 'Chrome 8');
INSERT INTO `log` VALUES (118, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:09:36', 1, 'Chrome 8');
INSERT INTO `log` VALUES (119, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:09:49', 0, 'Chrome 8');
INSERT INTO `log` VALUES (120, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:09:55', 0, 'Chrome 8');
INSERT INTO `log` VALUES (121, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:10:16', 2, 'Chrome 8');
INSERT INTO `log` VALUES (122, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:10:21', 1, 'Chrome 8');
INSERT INTO `log` VALUES (123, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-19 16:11:14', 1, 'Chrome 8');
INSERT INTO `log` VALUES (124, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:11:19', 2, 'Chrome 8');
INSERT INTO `log` VALUES (125, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-19 16:11:44', 1, 'Chrome 8');
INSERT INTO `log` VALUES (126, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-19 16:11:56', 1, 'Chrome 8');
INSERT INTO `log` VALUES (127, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '8', '0:0:0:0:0:0:0:1', '2020-09-19 16:13:22', 1, 'Chrome 8');
INSERT INTO `log` VALUES (128, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '7', '0:0:0:0:0:0:0:1', '2020-09-19 16:13:35', 1, 'Chrome 8');
INSERT INTO `log` VALUES (129, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-19 16:14:09', 1, 'Chrome 8');
INSERT INTO `log` VALUES (130, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-19 16:14:14', 1, 'Chrome 8');
INSERT INTO `log` VALUES (131, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '5', '0:0:0:0:0:0:0:1', '2020-09-19 16:14:38', 1, 'Chrome 8');
INSERT INTO `log` VALUES (132, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '5', '0:0:0:0:0:0:0:1', '2020-09-19 16:15:03', 1, 'Chrome 8');
INSERT INTO `log` VALUES (133, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '5', '0:0:0:0:0:0:0:1', '2020-09-19 16:26:24', 1, 'Chrome 8');
INSERT INTO `log` VALUES (134, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:26:29', 1, 'Chrome 8');
INSERT INTO `log` VALUES (135, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=9, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 16:37:44', 8, 'Chrome 8');
INSERT INTO `log` VALUES (136, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 16:37:58', 1, 'Chrome 8');
INSERT INTO `log` VALUES (137, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:38:15', 1, 'Chrome 8');
INSERT INTO `log` VALUES (138, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=发的说法是的, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:38:43', 2, 'Chrome 8');
INSERT INTO `log` VALUES (139, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=发的说法是的, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:38:46', 2, 'Chrome 8');
INSERT INTO `log` VALUES (140, '部门新增', 'admin', 'error', 'token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=发的说法是的, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:39:00', 1, 'Chrome 8');
INSERT INTO `log` VALUES (141, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 16:39:40', 9, 'Chrome 8');
INSERT INTO `log` VALUES (142, '部门新增', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=的撒打算, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:39:45', 1, 'Chrome 8');
INSERT INTO `log` VALUES (143, '部门新增', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:40:13', 1, 'Chrome 8');
INSERT INTO `log` VALUES (144, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 16:40:53', 9, 'Chrome 8');
INSERT INTO `log` VALUES (145, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:40:58', 1, 'Chrome 8');
INSERT INTO `log` VALUES (146, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:41:13', 2, 'Chrome 8');
INSERT INTO `log` VALUES (147, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨发, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:41:31', 0, 'Chrome 8');
INSERT INTO `log` VALUES (148, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:41:53', 1, 'Chrome 8');
INSERT INTO `log` VALUES (149, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:42:14', 1, 'Chrome 8');
INSERT INTO `log` VALUES (150, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=大叔大婶, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:44:01', 2, 'Chrome 8');
INSERT INTO `log` VALUES (151, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:44:32', 2, 'Chrome 8');
INSERT INTO `log` VALUES (152, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:44:50', 1, 'Chrome 8');
INSERT INTO `log` VALUES (153, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:45:11', 1, 'Chrome 8');
INSERT INTO `log` VALUES (154, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-19 16:46:42', 1, 'Chrome 8');
INSERT INTO `log` VALUES (155, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '5', '0:0:0:0:0:0:0:1', '2020-09-19 16:46:57', 1, 'Chrome 8');
INSERT INTO `log` VALUES (156, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=发顺丰的, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:47:15', 1, 'Chrome 8');
INSERT INTO `log` VALUES (157, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-19 16:47:24', 1, 'Chrome 8');
INSERT INTO `log` VALUES (158, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 16:47:28', 1, 'Chrome 8');
INSERT INTO `log` VALUES (159, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-19 16:48:50', 2, 'Chrome 8');
INSERT INTO `log` VALUES (160, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 16:48:53', 1, 'Chrome 8');
INSERT INTO `log` VALUES (161, '部门新增', 'admin', 'error', '认证失败，token过期或者token无效！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=撒发大水, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:48:58', 1, 'Chrome 8');
INSERT INTO `log` VALUES (162, '部门新增', 'admin', 'error', 'Token认证失败，原因：token无效或者过期！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=佛挡杀佛, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:50:03', 9, 'Chrome 8');
INSERT INTO `log` VALUES (163, '部门新增', 'admin', 'error', 'Token认证失败，原因：token无效或者过期！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=范德萨范德萨, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 16:50:14', 0, 'Chrome 8');
INSERT INTO `log` VALUES (164, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-19 16:50:24', 1, 'Chrome 8');
INSERT INTO `log` VALUES (165, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 16:50:27', 1, 'Chrome 8');
INSERT INTO `log` VALUES (166, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 16:50:55', 0, 'Chrome 8');
INSERT INTO `log` VALUES (167, '部门新增', 'admin', 'error', 'Token认证失败，原因：token无效或者过期！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=黑寡妇, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 17:07:04', 5, 'Chrome 8');
INSERT INTO `log` VALUES (168, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-19 17:07:13', 1, 'Chrome 8');
INSERT INTO `log` VALUES (169, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 17:07:19', 1, 'Chrome 8');
INSERT INTO `log` VALUES (170, '部门删除', 'admin', 'error', '部门删除失败', 'com.zmzxbd.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-19 17:07:31', 2, 'Chrome 8');
INSERT INTO `log` VALUES (171, '部门编辑', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.zmzxbd.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 17:07:34', 1, 'Chrome 8');
INSERT INTO `log` VALUES (172, '部门新增', 'admin', 'error', 'Token认证失败，原因：token无效或者过期！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=会更好, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 17:07:39', 0, 'Chrome 8');
INSERT INTO `log` VALUES (173, '部门新增', 'admin', 'error', 'Token认证失败，原因：token无效或者过期！', 'com.zmzxbd.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=讲话稿讲话稿, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 17:07:57', 1, 'Chrome 8');
INSERT INTO `log` VALUES (174, '部门删除', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-19 17:19:20', 262, 'Chrome 8');
INSERT INTO `log` VALUES (175, '部门删除', 'admin', 'info', NULL, 'com.zmzxbd.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-19 17:19:30', 2, 'Chrome 8');
INSERT INTO `log` VALUES (176, '部门新增', 'admin', 'error', 'Token认证失败，原因：token无效或者过期！', 'com.syyo.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=第三方, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 17:36:41', 10, 'Chrome 8');
INSERT INTO `log` VALUES (177, '部门编辑', 'admin', 'error', '部门添加失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 17:36:48', 0, 'Chrome 8');
INSERT INTO `log` VALUES (178, '部门删除', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.syyo.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-19 17:36:51', 0, 'Chrome 8');
INSERT INTO `log` VALUES (179, '部门删除', 'admin', 'error', '您没有该功能的权限，请联系管理员！', 'com.syyo.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-19 17:37:54', 5, 'Chrome 8');
INSERT INTO `log` VALUES (180, '部门编辑', 'admin', 'error', '部门添加失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-19 17:37:58', 2, 'Chrome 8');
INSERT INTO `log` VALUES (181, '部门新增', 'admin', 'error', 'Token认证失败，原因：token无效或者过期！', 'com.syyo.admin.controller.SysDeptController.add()', 'DeptReq(deptId=null, deptName=个发黑寡妇, pidList=[])', '0:0:0:0:0:0:0:1', '2020-09-19 17:38:01', 0, 'Chrome 8');
INSERT INTO `log` VALUES (182, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '9', '0:0:0:0:0:0:0:1', '2020-09-21 08:50:46', 6, 'Chrome 8');
INSERT INTO `log` VALUES (183, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '8', '0:0:0:0:0:0:0:1', '2020-09-21 08:51:46', 1, 'Chrome 8');
INSERT INTO `log` VALUES (184, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-21 08:52:10', 2, 'Chrome 8');
INSERT INTO `log` VALUES (185, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '7', '0:0:0:0:0:0:0:1', '2020-09-21 08:53:47', 0, 'Chrome 8');
INSERT INTO `log` VALUES (186, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '8', '0:0:0:0:0:0:0:1', '2020-09-21 08:56:16', 1, 'Chrome 8');
INSERT INTO `log` VALUES (187, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '5', '0:0:0:0:0:0:0:1', '2020-09-21 08:59:00', 1, 'Chrome 8');
INSERT INTO `log` VALUES (188, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '7', '0:0:0:0:0:0:0:1', '2020-09-21 09:00:06', 1, 'Chrome 8');
INSERT INTO `log` VALUES (189, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '8', '0:0:0:0:0:0:0:1', '2020-09-21 09:05:02', 1, 'Chrome 8');
INSERT INTO `log` VALUES (190, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '8', '0:0:0:0:0:0:0:1', '2020-09-21 09:05:09', 2, 'Chrome 8');
INSERT INTO `log` VALUES (191, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '8', '0:0:0:0:0:0:0:1', '2020-09-21 09:05:18', 1, 'Chrome 8');
INSERT INTO `log` VALUES (192, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '7', '0:0:0:0:0:0:0:1', '2020-09-21 09:10:51', 2, 'Chrome 8');
INSERT INTO `log` VALUES (193, '部门编辑', 'admin', 'error', '部门修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-21 09:11:33', 7, 'Chrome 8');
INSERT INTO `log` VALUES (194, '部门编辑', 'admin', 'error', '部门修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-21 09:11:49', 1, 'Chrome 8');
INSERT INTO `log` VALUES (195, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-21 09:12:14', 1, 'Chrome 8');
INSERT INTO `log` VALUES (196, '部门编辑', 'admin', 'error', '部门修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=6, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-21 09:12:19', 1, 'Chrome 8');
INSERT INTO `log` VALUES (197, '部门编辑', 'admin', 'error', '部门修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-21 09:13:39', 2, 'Chrome 8');
INSERT INTO `log` VALUES (198, '部门编辑', 'admin', 'error', '部门修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=7, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-21 09:13:53', 1, 'Chrome 8');
INSERT INTO `log` VALUES (199, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '5', '0:0:0:0:0:0:0:1', '2020-09-21 09:17:54', 2, 'Chrome 8');
INSERT INTO `log` VALUES (200, '部门编辑', 'admin', 'error', '部门修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=5, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-21 09:17:57', 1, 'Chrome 8');
INSERT INTO `log` VALUES (201, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-21 09:20:07', 1, 'Chrome 8');
INSERT INTO `log` VALUES (202, '部门编辑', 'admin', 'error', '部门修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=6, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-21 09:20:10', 1, 'Chrome 8');
INSERT INTO `log` VALUES (203, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '4', '0:0:0:0:0:0:0:1', '2020-09-21 09:20:51', 1, 'Chrome 8');
INSERT INTO `log` VALUES (204, '部门编辑', 'admin', 'error', '部门修改失败', 'com.syyo.admin.controller.SysDeptController.edit()', 'DeptReq(deptId=4, deptName=SYYO公司1, pidList=null)', '0:0:0:0:0:0:0:1', '2020-09-21 09:22:01', 1, 'Chrome 8');
INSERT INTO `log` VALUES (205, '部门删除', 'admin', 'error', '部门删除失败', 'com.syyo.admin.controller.SysDeptController.del()', '6', '0:0:0:0:0:0:0:1', '2020-09-21 09:22:09', 2, 'Chrome 8');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `dept_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门名称',
  `pid` int(11) DEFAULT NULL COMMENT '父级id',
  `pids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '父级id数组',
  `rank` tinyint(2) DEFAULT NULL COMMENT '层级',
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (4, 'SYYO公司1', 0, '0', 1, '2020-09-02 17:15:01', '2020-09-02 17:15:01');
INSERT INTO `sys_dept` VALUES (5, 'SYYO公司1', 0, '0', 1, '2020-09-02 17:15:31', '2020-09-02 17:15:31');
INSERT INTO `sys_dept` VALUES (6, 'SYYO公司1', 0, '0', 1, '2020-09-02 17:15:57', '2020-09-02 17:15:57');
INSERT INTO `sys_dept` VALUES (7, 'SYYO公司1', 0, '0', 1, '2020-09-02 17:17:26', '2020-09-02 17:17:26');
INSERT INTO `sys_dept` VALUES (8, 'SYYO公司1', 0, '0', 1, '2020-09-02 17:19:49', '2020-09-02 17:19:49');
INSERT INTO `sys_dept` VALUES (9, 'SYYO公司1', 0, '0', 1, '2020-09-02 17:26:47', '2020-09-02 17:26:47');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `pid` int(10) DEFAULT NULL COMMENT '父级id',
  `pids` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '父级id数组',
  `perms` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '后端权限标识',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '前端跳转路由',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '前端组件地址',
  `component_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '前端组件name',
  `hidden` bit(1) DEFAULT NULL COMMENT '是否隐藏',
  `cache` bit(1) DEFAULT NULL COMMENT '是否缓存',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图标',
  `type` tinyint(2) DEFAULT NULL COMMENT '类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, '0', NULL, 'system', NULL, NULL, b'0', b'0', 'system', 1, 1, '2020-08-12 12:17:41', '2020-07-20 10:01:12', NULL);
INSERT INTO `sys_menu` VALUES (2, '用户管理', 1, '1', NULL, 'user', 'system/user/index', 'User', b'0', b'0', 'user', 2, 2, '2020-08-22 14:53:19', '2020-07-20 10:02:12', NULL);
INSERT INTO `sys_menu` VALUES (3, '用户添加', 2, '1,2', 'system:user:add', NULL, NULL, NULL, b'1', b'0', NULL, 3, 2, '2020-08-12 17:36:05', '2020-07-20 10:03:49', NULL);
INSERT INTO `sys_menu` VALUES (4, '用户删除', 2, '1,2', 'system:user:del', NULL, NULL, NULL, b'1', b'0', NULL, 3, 3, '2020-08-12 17:36:17', '2020-07-20 10:04:18', NULL);
INSERT INTO `sys_menu` VALUES (5, '用户编辑', 2, '1,2', 'system:user:edit', NULL, NULL, NULL, b'1', b'0', NULL, 3, 4, '2020-08-12 17:36:27', '2020-07-20 10:04:26', NULL);
INSERT INTO `sys_menu` VALUES (6, '用户查询', 2, '1,2', NULL, NULL, NULL, NULL, b'1', b'0', NULL, 3, 5, '2020-07-20 10:04:38', '2020-07-20 10:04:38', NULL);
INSERT INTO `sys_menu` VALUES (7, '角色管理', 1, '1', NULL, 'role', 'system/role/index', 'Role', b'0', b'0', 'role', 2, 3, '2020-08-22 14:53:31', '2020-07-20 10:05:43', NULL);
INSERT INTO `sys_menu` VALUES (8, '角色添加', 7, '1,7', 'system:role:add', NULL, NULL, NULL, b'1', b'0', NULL, 3, 2, '2020-08-12 17:36:54', '2020-07-20 10:06:12', NULL);
INSERT INTO `sys_menu` VALUES (9, '角色删除', 7, '1,7', 'system:role:del', NULL, NULL, NULL, b'1', b'0', NULL, 3, 3, '2020-08-12 17:37:04', '2020-07-20 10:06:24', NULL);
INSERT INTO `sys_menu` VALUES (10, '角色编辑', 7, '1,7', 'system:role:edit', NULL, NULL, NULL, b'1', b'0', NULL, 3, 4, '2020-08-12 17:37:14', '2020-07-20 10:06:31', NULL);
INSERT INTO `sys_menu` VALUES (11, '角色查询', 7, '1,7', NULL, NULL, NULL, NULL, b'1', b'0', NULL, 3, 5, '2020-07-20 10:06:36', '2020-07-20 10:06:36', NULL);
INSERT INTO `sys_menu` VALUES (12, '菜单管理', 1, '1', NULL, 'menu', 'system/menu/index', 'Menu', b'0', b'0', 'tree-table', 2, 4, '2020-08-22 14:53:38', '2020-07-20 10:07:58', NULL);
INSERT INTO `sys_menu` VALUES (13, '菜单添加', 12, '1,12', 'system:menu:add', NULL, NULL, NULL, b'1', b'0', NULL, 3, 2, '2020-08-12 17:39:18', '2020-07-20 10:08:27', NULL);
INSERT INTO `sys_menu` VALUES (14, '菜单删除', 12, '1,12', 'system:menu:del', NULL, NULL, NULL, b'1', b'0', NULL, 3, 3, '2020-08-12 17:39:30', '2020-07-20 10:08:33', NULL);
INSERT INTO `sys_menu` VALUES (15, '菜单编辑', 12, '1,12', 'system:menu:edit', NULL, NULL, NULL, b'1', b'0', NULL, 3, 4, '2020-08-12 17:39:41', '2020-07-20 10:08:37', NULL);
INSERT INTO `sys_menu` VALUES (16, '菜单查询', 12, '1,12', NULL, NULL, NULL, NULL, b'1', b'0', NULL, 3, 5, '2020-07-20 10:08:42', '2020-07-20 10:08:42', NULL);
INSERT INTO `sys_menu` VALUES (17, ' 系统工具', 0, '0', NULL, 'sys-tools', NULL, NULL, b'0', b'0', 'sys-tools', 1, 10, '2020-08-20 11:17:43', '2020-09-04 12:31:31', NULL);
INSERT INTO `sys_menu` VALUES (21, '代码生成', 17, '17', NULL, 'generator', 'sys-tools/generator/index', 'Generator', b'0', b'0', 'dev', 2, 10, '2020-08-20 11:19:04', '2020-09-04 09:33:13', NULL);
INSERT INTO `sys_menu` VALUES (33, '系统监控', 0, '0', NULL, 'monitor', NULL, NULL, b'0', b'0', 'mnt', 1, 30, '2020-08-22 14:27:27', '2020-09-04 09:31:42', NULL);
INSERT INTO `sys_menu` VALUES (34, '角色授权', 7, '1,7', 'system:role:auth', NULL, NULL, NULL, b'0', b'0', NULL, 3, 6, '2020-08-12 17:37:57', '2020-08-12 17:37:57', NULL);
INSERT INTO `sys_menu` VALUES (35, '用户导入', 2, '1,2', 'system:user:import', NULL, NULL, NULL, b'0', b'0', NULL, 3, 5, '2020-08-12 17:38:39', '2020-08-12 17:38:39', NULL);
INSERT INTO `sys_menu` VALUES (36, '用户导出', 2, '1,2', 'system:user:export', NULL, NULL, NULL, b'0', b'0', NULL, 3, 6, '2020-08-12 17:39:03', '2020-08-12 17:39:03', NULL);
INSERT INTO `sys_menu` VALUES (37, '部门管理', 1, '1', NULL, 'dept', 'system/dept/index', 'Dept', b'0', b'0', 'tree', 2, 5, '2020-08-22 14:53:51', '2020-08-26 11:43:06', NULL);
INSERT INTO `sys_menu` VALUES (38, '部门添加', 37, '1,37', 'system:dept:add', NULL, NULL, NULL, b'0', b'0', NULL, 3, 1, '2020-08-20 11:10:35', '2020-08-20 11:06:15', NULL);
INSERT INTO `sys_menu` VALUES (39, '部门删除', 37, '1,37', 'system:dept:del', NULL, NULL, NULL, b'0', b'0', NULL, 3, 2, '2020-08-20 11:10:53', '2020-08-20 11:09:38', NULL);
INSERT INTO `sys_menu` VALUES (40, '部门编辑', 37, '1,37', 'system:dept:edit', NULL, NULL, NULL, b'0', b'0', NULL, 3, 3, '2020-08-20 11:11:01', '2020-08-20 11:09:53', NULL);
INSERT INTO `sys_menu` VALUES (41, '部门查询', 37, '1,37', NULL, NULL, NULL, NULL, b'0', b'0', NULL, 3, 4, '2020-08-20 11:10:11', '2020-08-20 11:10:11', NULL);
INSERT INTO `sys_menu` VALUES (51, '生成配置', 17, '17', NULL, 'generator/config/:tableName', 'sys-tools/generator/config', 'GeneratorConfig', b'1', b'0', NULL, 2, 11, '2020-08-26 15:35:19', '2020-09-04 09:33:23', NULL);
INSERT INTO `sys_menu` VALUES (52, '生成预览', 17, '17', NULL, 'generator/preview/:tableName', 'sys-tools/generator/preview', 'Preview', b'1', b'0', NULL, 2, 12, '2020-08-26 15:35:36', '2020-09-04 09:33:30', NULL);
INSERT INTO `sys_menu` VALUES (53, '接口文档', 17, '17', NULL, 'swagger', 'sys-tools/swagger/index', 'Swagger', b'0', b'0', 'education', 2, 13, '2020-09-03 11:37:10', '2020-09-04 09:33:37', NULL);
INSERT INTO `sys_menu` VALUES (54, '操作日志', 33, '33', NULL, 'log', 'monitor/log/index', 'Log', b'0', b'0', 'log', 2, 30, '2020-09-03 17:37:29', '2020-09-04 09:34:42', NULL);
INSERT INTO `sys_menu` VALUES (55, '错误日志', 33, '33', NULL, 'errorLog', 'monitor/log/errorLog', 'ErrorLog', b'0', b'0', 'error', 2, 31, '2020-09-03 17:39:01', '2020-09-04 09:34:48', NULL);
INSERT INTO `sys_menu` VALUES (56, '组件管理', 0, '0', NULL, 'components', NULL, NULL, b'0', b'0', 'zujian', 1, 50, '2020-09-04 09:29:58', '2020-09-04 09:31:57', NULL);
INSERT INTO `sys_menu` VALUES (57, '图标库', 56, '56', NULL, 'icons', 'components/icons/index', 'Icons', b'0', b'0', 'icon', 2, 50, '2020-09-04 09:36:32', '2020-09-04 11:42:27', NULL);
INSERT INTO `sys_menu` VALUES (59, '动态table', 56, '56', NULL, 'dynamicTable', 'components/table/dynamic-table', 'DynamicTable', b'0', b'0', 'tab', 2, 51, '2020-09-04 11:37:21', '2020-09-04 12:23:52', NULL);
INSERT INTO `sys_menu` VALUES (61, '多级菜单', 0, '0', NULL, 'nested', NULL, NULL, b'0', b'0', 'menu', 1, 70, '2020-09-14 16:26:49', '2020-09-14 16:26:49', NULL);
INSERT INTO `sys_menu` VALUES (62, '二级菜单1', 61, '61', NULL, 'menu1', 'nested/menu1/index', 'Menu1', b'0', b'0', 'menu', 2, 71, '2020-09-14 16:27:59', '2020-09-14 17:00:58', NULL);
INSERT INTO `sys_menu` VALUES (63, '二级菜单2', 61, '61', NULL, 'menu2', 'nested/menu2/index', 'Menu2', b'0', b'0', 'menu', 2, 72, '2020-09-14 16:29:01', '2020-09-14 17:01:06', NULL);
INSERT INTO `sys_menu` VALUES (64, '三级菜单1', 62, '61,62', NULL, 'menu1-1', 'nested/menu1/menu1-1', NULL, b'0', b'0', 'menu', 2, 711, '2020-09-14 16:29:52', '2020-09-14 16:29:52', NULL);
INSERT INTO `sys_menu` VALUES (65, '三级菜单2', 62, '61,62', NULL, 'menu1-2', 'nested/menu1/menu1-2', NULL, b'0', b'0', 'menu', 2, 712, '2020-09-14 16:30:26', '2020-09-14 17:10:35', NULL);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '通告id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '通告标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '通告内容',
  `type` tinyint(2) DEFAULT NULL COMMENT '通告类型',
  `release_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发布人',
  `release_date` datetime(0) DEFAULT NULL COMMENT '发布时间',
  `create_date` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (8, '标题', '内容', 1, NULL, NULL, '2020-07-17 16:52:45', NULL);
INSERT INTO `sys_notice` VALUES (9, '标题', '内容', 1, NULL, NULL, '2020-07-17 16:53:41', NULL);
INSERT INTO `sys_notice` VALUES (10, '标题1', '内容', 1, NULL, NULL, '2020-07-17 16:58:55', NULL);
INSERT INTO `sys_notice` VALUES (11, '张三', '内容', 1, NULL, NULL, '2020-07-17 16:59:05', NULL);
INSERT INTO `sys_notice` VALUES (12, '张三1', '内容', 1, NULL, NULL, '2020-07-17 16:59:07', NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `role_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限标识',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', '2020-07-20 09:58:09', '2020-07-20 09:58:09', '备注');
INSERT INTO `sys_role` VALUES (2, '管理员', 'syyo', '2020-08-22 11:21:40', '2020-08-22 11:21:40', NULL);
INSERT INTO `sys_role` VALUES (4, 'juese1', 'key1', '2020-09-09 10:12:35', '2020-09-09 10:12:35', NULL);
INSERT INTO `sys_role` VALUES (7, '测试1', 'ceshi', '2020-09-09 10:15:28', '2020-09-09 10:15:28', NULL);
INSERT INTO `sys_role` VALUES (8, '测试2', 'ceshi', '2020-09-09 10:17:34', '2020-09-09 10:17:34', NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` int(10) NOT NULL,
  `menu_id` int(10) NOT NULL,
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单中间表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (1, 3);
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 7);
INSERT INTO `sys_role_menu` VALUES (1, 8);
INSERT INTO `sys_role_menu` VALUES (1, 9);
INSERT INTO `sys_role_menu` VALUES (1, 10);
INSERT INTO `sys_role_menu` VALUES (1, 11);
INSERT INTO `sys_role_menu` VALUES (1, 12);
INSERT INTO `sys_role_menu` VALUES (1, 13);
INSERT INTO `sys_role_menu` VALUES (1, 14);
INSERT INTO `sys_role_menu` VALUES (1, 15);
INSERT INTO `sys_role_menu` VALUES (1, 16);
INSERT INTO `sys_role_menu` VALUES (1, 17);
INSERT INTO `sys_role_menu` VALUES (1, 21);
INSERT INTO `sys_role_menu` VALUES (1, 33);
INSERT INTO `sys_role_menu` VALUES (1, 34);
INSERT INTO `sys_role_menu` VALUES (1, 35);
INSERT INTO `sys_role_menu` VALUES (1, 36);
INSERT INTO `sys_role_menu` VALUES (1, 37);
INSERT INTO `sys_role_menu` VALUES (1, 38);
INSERT INTO `sys_role_menu` VALUES (1, 39);
INSERT INTO `sys_role_menu` VALUES (1, 40);
INSERT INTO `sys_role_menu` VALUES (1, 41);
INSERT INTO `sys_role_menu` VALUES (1, 51);
INSERT INTO `sys_role_menu` VALUES (1, 52);
INSERT INTO `sys_role_menu` VALUES (1, 53);
INSERT INTO `sys_role_menu` VALUES (1, 54);
INSERT INTO `sys_role_menu` VALUES (1, 55);
INSERT INTO `sys_role_menu` VALUES (1, 56);
INSERT INTO `sys_role_menu` VALUES (1, 57);
INSERT INTO `sys_role_menu` VALUES (1, 59);
INSERT INTO `sys_role_menu` VALUES (1, 61);
INSERT INTO `sys_role_menu` VALUES (1, 62);
INSERT INTO `sys_role_menu` VALUES (1, 63);
INSERT INTO `sys_role_menu` VALUES (1, 64);
INSERT INTO `sys_role_menu` VALUES (1, 65);
INSERT INTO `sys_role_menu` VALUES (2, 38);
INSERT INTO `sys_role_menu` VALUES (2, 40);
INSERT INTO `sys_role_menu` VALUES (2, 41);
INSERT INTO `sys_role_menu` VALUES (3, 3);
INSERT INTO `sys_role_menu` VALUES (3, 4);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户账号',
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `sex` tinyint(2) DEFAULT NULL COMMENT '性别',
  `pass_word` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态（1：正常，0：禁用）',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', 'zhong1', '13800000000', '78766003@qq.com', 1, '$2a$10$/6Vxp37Wo8dO6iSQXxwdWeRRkXFtX2.htjs7bqW.5odAT54b0kUZi', 1, 'F:\\com\\syyo\\files\\image\\ceshi01-20200819114846675.jpg', '2020-07-20 09:57:40', '2020-07-20 09:57:40', NULL);
INSERT INTO `sys_user` VALUES (2, 'wang1', 'dasd1', '13800000000', 'com2', 1, '$2a$10$3HjZ5DKpe8z5ZKSSunDh5uG6GRxI4qSZej9uDlNvjEXd13In3DS3q', 1, '/F:/com/syyo/images/f778738c-e4f8-4870-b634-56703b4acafe.gif', '2020-07-20 14:36:30', '2020-07-20 15:25:49', '1113');
INSERT INTO `sys_user` VALUES (3, 'wang2', 'aaa', '13800000000', 'qq', 1, '$2a$10$4Vwq268XpS3iMl07zM5hj.BdHBQE52Xy4SZwgPJIu2uYmchG/Ple.', 1, '/F:/com/syyo/images/f778738c-e4f8-4870-b634-56703b4acafe.gif', '2020-07-20 14:38:09', '2020-08-22 11:22:15', '11');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色中间表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 1);
INSERT INTO `sys_user_role` VALUES (3, 2);
INSERT INTO `sys_user_role` VALUES (4, 1);

SET FOREIGN_KEY_CHECKS = 1;
