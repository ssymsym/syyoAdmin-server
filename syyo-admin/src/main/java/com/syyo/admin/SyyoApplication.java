package com.syyo.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * @Auther: wangzhong
 * @Date: 2019/12/3 11:47
 * @Description:
 */
@ComponentScan(basePackages = {"com.syyo.admin", "com.syyo.generator","com.syyo.common","com.syyo.logging"})//多个模块的controller扫描的包
@MapperScan({"com.syyo.admin.mapper","com.syyo.generator.mapper","com.syyo.logging.mapper"})//多个模块的mapper扫描的包
@SpringBootApplication
public class SyyoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SyyoApplication.class,args);

        System.out.println("       启动成功                 \n" +
                "    '----'        ||      //        \n" +
                "  //      \\\\      ||    //      \n" +
                " ||        ||     ||   //          \n" +
                " ||        ||     ||   \\\\           \n" +
                "  \\\\      //      ||    \\\\             \n" +
                "    '----'        ||      \\\\              ");

    }
}
