//package com.syyo.admin.common.constant;
//
//import lombok.Data;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @Auther: wangzhong
// * @Date: 2019/10/21 16:17
// * @Description: 系统配置类
// */
//@Data
////@Configuration
//public class SettingsConstant {
//
//    /*    ========= 分页 ===========  */
//    @Value("${settings.page.num}")
//    private String pageNum; //当前页
//    @Value("${settings.page.size}")
//    private String pageSize; //每页条数
//    @Value("${settings.page.range}")
//    private String pageRange; //范围
//
//    /*    ========= 文件 ===========  */
//    @Value("${settings.file.image}")
//    private String imagePath; //图片路劲
//    @Value("${settings.file.documents}")
//    private String documentsPath; //文档路劲
//    @Value("${settings.file.music}")
//    private String musicPath; //音乐路劲
//    @Value("${settings.file.video}")
//    private String videoPath; //视频路劲
//    @Value("${settings.file.other}")
//    private String otherPath; //其他路劲
//
//    /*    ========= security开关 ===========  */
//    @Value("${settings.securitySwitch}")
//    private Integer securitySwitch; //securitySwitch 开关
//
//}
