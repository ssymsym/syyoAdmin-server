package com.syyo.admin.common.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.syyo.common.domain.ResultVo;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 自定义权限校验处理器
 */
@Component
public class MyAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request,
                       HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException {

        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ResultVo<Object> resultVo = new ResultVo<>();
        resultVo.setCode(90003);
        resultVo.setMessage("没有权限");
        out.write(new ObjectMapper().writeValueAsString(resultVo));
        out.flush();
        out.close();
    }
}
