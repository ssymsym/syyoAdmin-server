package com.syyo.admin.common.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.syyo.common.domain.ResultVo;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 自定义认证处理器
 */
@Component
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {

        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ResultVo<Object> resultVo = new ResultVo<>();
        resultVo.setCode(90001);
        resultVo.setMessage("认证失败，原因：账号或密码错误 || 没有分配角色 || token解析失败");
        out.write(new ObjectMapper().writeValueAsString(resultVo));
        out.flush();
        out.close();
    }
}
