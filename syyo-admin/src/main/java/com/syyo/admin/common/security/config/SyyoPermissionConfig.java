package com.syyo.admin.common.security.config;

import com.syyo.common.utils.SecurityUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author
 *
 * 自定义注解的权限校验的类
 *
 */
@Service(value = "syyo")
public class SyyoPermissionConfig {

    public Boolean check(String ...permissions){
        // 获取当前用户的所有权限
        List<String> elPermissions = SecurityUtils.getUserDetails().getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        boolean b  = true;
        //判断当前用户是否是admin，admin有所有权限
        if (!elPermissions.contains("admin")) {
            //判断当前请求的权限标识是否在当前用户的所有权限中
            //permissions集合里的值如果在elPermissions集合里只要包含一个，就返回true
            b = Arrays.stream(permissions).anyMatch(elPermissions::contains);
        }
        return b;
    }
}
