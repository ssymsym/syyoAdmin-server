package com.syyo.admin.common.security.config;

import com.syyo.common.constant.SettingsConstant;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * 将token的Token过滤器添加到security配置中
 */
public class TokenConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private final TokenProvider tokenProvider;
    private final SettingsConstant settingsConstant;

    public TokenConfigurer(TokenProvider tokenProvider,SettingsConstant settingsConstant){
        this.tokenProvider = tokenProvider;
        this.settingsConstant = settingsConstant;
    }

    @Override
    public void configure(HttpSecurity http) {
        TokenFilter customFilter = new TokenFilter(tokenProvider,settingsConstant);
        http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
