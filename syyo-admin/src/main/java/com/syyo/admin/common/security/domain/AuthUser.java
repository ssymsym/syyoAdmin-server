package com.syyo.admin.common.security.domain;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 登录接口参数
 */
@Data
public class AuthUser {

    @NotNull(message = "账号不能为空")
    private String username;
    @NotNull(message = "密码不能为空")
    private String password;
    @NotNull(message = "验证码不能为空")
    private String code;
    private String uuid;
}
