package com.syyo.admin.common.security.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @Auther: wangzhong
 * @Date: 2019/9/30 17:20
 * @Description: 用户返回实体类
 */
@Getter
@AllArgsConstructor
public class JwtUser implements UserDetails {

    private final Integer id;
    private final String username;
    @JsonIgnore
    private final String password;
    private final String nickName;
    private final Integer deptId;
    private final String phone;
    private final String email;
    private final Integer sex;
    private final String avatar;
    private final boolean enabled;

    @JsonIgnore //标识这个属性不返回
    private final Collection<GrantedAuthority> authorities;

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Collection getRoles() {
        //获取用户角色信息，封装成set
        return authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
    }
}
