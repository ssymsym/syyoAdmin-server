package com.syyo.admin.common.security.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.syyo.admin.common.security.domain.JwtUser;
import com.syyo.common.enums.ResultEnum;
import com.syyo.common.exception.SysException;

import com.syyo.admin.domain.entity.SysUser;
import com.syyo.admin.mapper.SysUserMapper;
import com.syyo.admin.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * security的service，实现loadUserByUsername方法
 *
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private SysRoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String username)   {

        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name", username);
        SysUser user = userMapper.selectOne(wrapper);
        if (user == null) {
            throw new SysException(ResultEnum.E_80006.getCode(), ResultEnum.E_80006.getMessage());
        } else {
            //用户禁用
            if ( 0 == user.getStatus()) {
                throw new SysException(ResultEnum.E_80007.getCode(), ResultEnum.E_80007.getMessage());
            }
        }
        return createJwtUser(user);
    }

    /**
     * 创建jwtUser对象，封装
     * @param user
     * @return
     */
    private UserDetails createJwtUser(SysUser user) {
        return new JwtUser(
                user.getUserId(),
                user.getUserName(),
                user.getPassWord(),
                user.getNickName(),
                user.getDeptId(),
                user.getPhone(),
                user.getEmail(),
                user.getSex(),
                user.getAvatar(),
                true,
                roleService.findByUsersId(user.getUserId())
        );
    }

}
