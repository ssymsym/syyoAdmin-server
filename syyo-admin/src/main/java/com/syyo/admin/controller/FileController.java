package com.syyo.admin.controller;

import com.syyo.admin.service.FileService;
import com.syyo.common.domain.FileUpload;
import com.syyo.common.domain.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/18 10:17
 * @Description:
 */
@Api(tags = "系统：文件模块")
@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    /**
     * 上传
     * @param file
     */
    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public ResultVo<FileUpload> upload(MultipartFile file) {
        return fileService.uploadFile(file);
    }

}
