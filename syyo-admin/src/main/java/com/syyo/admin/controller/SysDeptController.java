package com.syyo.admin.controller;


import com.syyo.admin.domain.req.DeptReq;
import com.syyo.admin.service.SysDeptService;
import com.syyo.common.domain.ResultVo;
import com.syyo.common.anno.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author wang
 * @since 2020-08-20
 */
@Api(tags = "系统：部门管理")
@RestController
@RequestMapping("/dept")
public class SysDeptController {

    @Autowired
    private SysDeptService deptService;

    /**
     * 部门新增
     * @param req
     * @return
     */
    @ApiOperation("部门新增")
    @SysLog("部门新增")
    @PreAuthorize("@syyo.check('system:dept:add')")
    @PostMapping
    public ResultVo add(@RequestBody DeptReq req){
        return deptService.add(req);
    }

    /**
     * 部门删除
     * @param
     * @return
     */
    @SysLog("部门删除")
    @ApiOperation("部门删除")
    @PreAuthorize("@syyo.check('system:dept:del')")
    @DeleteMapping("/{id}")
    public ResultVo del(@PathVariable("id")Integer id){
        return deptService.del(id);
    }

    /**
     * 部门编辑
     * @param req
     * @return
     */
    @SysLog("部门编辑")
    @ApiOperation("部门编辑")
    @PreAuthorize("@syyo.check('system:dept:edit')")
    @PutMapping
    public ResultVo edit(@RequestBody DeptReq req){
        return deptService.edit(req);
    }


    /**
     * 部门详情
     * @param
     * @return
     */
    @ApiOperation("部门详情")
    @GetMapping("/{id}")
    public ResultVo findOne(@PathVariable("id")Integer id){
        return deptService.findOne(id);
    }

    /**
     * 部门列表
     * @param
     * @return
     */
    @ApiOperation("部门列表")
    @GetMapping("/tree")
    public ResultVo tree(){
        return deptService.tree();
    }
}
