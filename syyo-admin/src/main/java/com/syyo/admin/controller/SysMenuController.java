package com.syyo.admin.controller;


import com.syyo.admin.domain.req.MenuReq;
import com.syyo.admin.service.SysMenuService;
import com.syyo.common.anno.SysLog;
import com.syyo.common.domain.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
@Api(tags = "系统：菜单管理")
@RestController
@RequestMapping("/menu")
public class SysMenuController {

    @Autowired
    private SysMenuService menuService;

    /**
     * 菜单新增
     * @param req
     * @return
     */
    @SysLog("菜单新增")
    @ApiOperation("菜单新增")
    @PreAuthorize("@syyo.check('system:menu:add')")
    @PostMapping
    public ResultVo add(@RequestBody MenuReq req){
        return menuService.add(req);
    }

    /**
     * 菜单删除
     * @param id
     * @return
     */
    @SysLog("菜单删除")
    @ApiOperation("菜单删除")
    @PreAuthorize("@syyo.check('system:menu:del')")
    @DeleteMapping("/{id}")
    public ResultVo del(@PathVariable("id")Integer id){
        return menuService.del(id);
    }

    /**
     * 菜单编辑
     * @param req
     * @return
     */
    @SysLog("菜单编辑")
    @ApiOperation("菜单编辑")
    @PreAuthorize("@syyo.check('system:menu:edit')")
    @PutMapping
    public ResultVo edit(@RequestBody MenuReq req){
        return menuService.edit(req);
    }


    /**
     * 菜单详情
     * @param
     * @return
     */
    @ApiOperation("菜单详情")
    @GetMapping("/{id}")
    public ResultVo findOne(@PathVariable("id")Integer id){
        return menuService.findOne(id);
    }

    /**
     * 菜单列表
     * @param
     * @return
     */
    @ApiOperation("菜单列表")
    @GetMapping("/tree")
    public ResultVo tree(){
        return menuService.tree();
    }

    /**
     * 菜单构造
     * @param
     * @return
     */
    @ApiOperation("菜单构造")
    @GetMapping("/build")
    public ResultVo buildMenu(){
        return menuService.buildMenu();
    }


}
