package com.syyo.admin.controller;


import com.syyo.admin.domain.req.RoleReq;
import com.syyo.admin.service.SysRoleService;
import com.syyo.common.anno.SysLog;
import com.syyo.common.domain.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
@Api(tags = "系统：角色管理")
@RestController
@RequestMapping("/role")
public class SysRoleController {

    @Autowired
    private SysRoleService roleService;

    /**
     * 角色新增
     *
     * @param req
     * @return
     */
    @SysLog("角色新增")
    @ApiOperation("角色新增")
    @PreAuthorize("@syyo.check('system:role:add')")
    @PostMapping
    public ResultVo add(@RequestBody RoleReq req) {
        return roleService.add(req);
    }

    /**
     * 角色删除
     *
     * @param id
     * @return
     */
    @SysLog("角色删除")
    @ApiOperation("角色删除")
    @PreAuthorize("@syyo.check('system:role:del')")
    @DeleteMapping("/{id}")
    public ResultVo del(@PathVariable("id")Integer id) {
        return roleService.del(id);
    }

    /**
     * 角色编辑
     *
     * @param req
     * @return
     */
    @SysLog("角色编辑")
    @ApiOperation("角色编辑")
    @PreAuthorize("@syyo.check('system:role:edit')")
    @PutMapping
    public ResultVo edit(@RequestBody RoleReq req) {
        return roleService.edit(req);
    }


    /**
     * 角色详情
     *
     * @param
     * @return
     */
    @ApiOperation("角色详情")
    @GetMapping("/{id}")
    public ResultVo findOne(@PathVariable("id") Integer id) {
        return roleService.findOne(id);
    }

    /**
     * 角色列表
     *
     * @param req
     * @return
     */
    @ApiOperation("角色列表")
    @GetMapping("/list/{pageNum}/{pageSize}")
    public ResultVo findAll(@PathVariable("pageNum") Integer pageNum,
                            @PathVariable("pageSize") Integer pageSize,
                            RoleReq req) {
        return roleService.findAll(pageNum, pageSize, req);
    }

    /**
     * 角色分配权限
     *
     * @param req
     * @return
     */
    @SysLog("角色分配权限")
    @ApiOperation("角色分配权限")
    @PreAuthorize("@syyo.check('system:role:auth')")
    @PutMapping("/auth")
    public ResultVo auth(@RequestBody RoleReq req) {
        return roleService.auth(req);
    }
}
