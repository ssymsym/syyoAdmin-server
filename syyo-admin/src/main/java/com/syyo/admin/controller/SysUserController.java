package com.syyo.admin.controller;


import com.syyo.admin.domain.excel.UserExport;
import com.syyo.admin.domain.req.UserReq;
import com.syyo.admin.service.SysUserService;
import com.syyo.common.anno.DataAuthorize;
import com.syyo.common.anno.SysLog;
import com.syyo.common.domain.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
@Api(tags = "系统：用户管理")
@RestController
@RequestMapping("/user")
public class SysUserController {


    @Autowired
    private SysUserService userService;

    /**
     * 用户新增
     * @param req
     * @return
     */
    @DataAuthorize
    @SysLog("用户新增")
    @ApiOperation("用户新增")
    @PreAuthorize("@syyo.check('system:user:add')")
    @PostMapping("/{deptId}")
    public ResultVo add(@RequestBody UserReq req,@PathVariable("deptId")Integer deptId){
        return userService.add(req,deptId);
    }

    /**
     * 用户删除
     * @param id
     * @return
     */
    @DataAuthorize
    @SysLog("用户删除")
    @ApiOperation("用户删除")
    @PreAuthorize("@syyo.check('system:user:del')")
    @DeleteMapping("/{id}/{deptId}")
    public ResultVo del(@PathVariable("id")Integer id,@PathVariable("deptId")Integer deptId){
        return userService.del(id,deptId);
    }

    /**
     * 用户编辑
     * @param req
     * @return
     */
    @DataAuthorize
    @SysLog("用户编辑")
    @ApiOperation("用户编辑")
    @PreAuthorize("@syyo.check('system:user:edit')")
    @PutMapping("/{deptId}")
    public ResultVo edit(@RequestBody UserReq req,@PathVariable("deptId")Integer deptId){
        return userService.edit(req,deptId);
    }


    /**
     * 用户详情
     * @param
     * @return
     */
    @ApiOperation("用户详情")
    @GetMapping("/{id}")
    public ResultVo findOne(@PathVariable("id")Integer id){
        return userService.findOne(id);
    }

    /**
     * 用户列表
     * @param req
     * @return
     */
    @DataAuthorize
    @ApiOperation("用户列表")
    @GetMapping("/list/{pageNum}/{pageSize}/{deptId}")
    public ResultVo findAll(@PathVariable("pageNum") Integer pageNum,
                            @PathVariable("pageSize") Integer pageSize,
                            UserReq req,@PathVariable("deptId")Integer deptId){
        return userService.findAll(pageNum,pageSize,req,deptId);
    }

    /**
     * 用户导入
     * @param
     * @return
     */
    @SysLog("用户导入")
    @ApiOperation("用户导入")
    @PreAuthorize("@syyo.check('system:user:import')")
    @PostMapping("/import")
    public ResultVo importUser(MultipartFile file) throws IOException {
        return userService.importUser(file);
    }

    /**
     * 用户导出
     * @param
     * @return
     */
    @SysLog("用户导出")
    @ApiOperation("用户导出")
    @PreAuthorize("@syyo.check('system:user:export')")
    @PostMapping("/export")
    public ResultVo exportUser(UserExport userExport, HttpServletResponse response){
        return userService.exportUser(userExport,response);
    }

    /**
     * 用户修改密码
     * @param req
     * @return
     */
    @SysLog("用户修改密码")
    @ApiOperation("用户修改密码")
    @PutMapping("/pwd")
    public ResultVo editPwd(@RequestBody UserReq req){
        return userService.editPwd(req);
    }

    /**
     * 用户修改头像
     * @param
     * @return
     */
    @SysLog("用户修改头像")
    @ApiOperation("用户修改头像")
    @PostMapping("/avatar")
    public ResultVo editAvatar(MultipartFile file){
        return userService.editAvatar(file);
    }
}
