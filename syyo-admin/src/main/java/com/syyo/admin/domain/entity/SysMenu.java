package com.syyo.admin.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
@Getter
@Setter
@Accessors(chain = true)
public class SysMenu extends Model<SysMenu> implements Comparable<Object>{

    private static final long serialVersionUID = 1L;

    /**
     * 菜单id
     */
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 父级id
     */
    private Integer pid;

    /**
     * 父级id数组
     */
    private String pids;

    /**
     * 后端权限标识
     */
    private String perms;

    /**
     * 前端跳转路由
     */
    private String path;

    /**
     * 前端组件地址
     */
    private String component;

    /**
     * 前端组件name
     */
    private String componentName;

    /**
     * 是否隐藏
     */
    private Boolean hidden;

    /**
     * 是否缓存
     */
    private Boolean cache;

    /**
     * 图标
     */
    private String icon;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    private String remark;

    @Override
    protected Serializable pkVal() {
        return this.menuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SysMenu menuEntity = (SysMenu) o;
        return Objects.equals(menuId, menuEntity.menuId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(menuId);
    }

    @Override
    public int compareTo(Object arg0) {
        SysMenu sysMenu = (SysMenu)arg0;
		// 根据 sort 排序
        return sort.compareTo(sysMenu.getSort());
    }
}
