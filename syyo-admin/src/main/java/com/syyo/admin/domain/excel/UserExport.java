package com.syyo.admin.domain.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/6 12:23
 * @Description: excel用户导出类
 */
@Data
public class UserExport {

    @ExcelProperty("用户账号")
    private String userName;//用户账号
    @ExcelProperty("昵称")
    private String nickName;//昵称
    @ExcelProperty("手机")
    private String phone;//手机
    @ExcelProperty("邮箱")
    private String email;//邮箱
}
