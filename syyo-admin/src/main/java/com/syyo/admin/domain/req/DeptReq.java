package com.syyo.admin.domain.req;

import lombok.Data;

import java.util.List;


/**
 * @Auther: wangzhong
 * @Date: 2019/9/21 17:23
 * @Description: 菜单请求实体类
 */
@Data
public class DeptReq {

    private Integer deptId; //菜单id
    private String deptName;//菜单名称
    private List<Integer> pidList;//父级id列表，前端接收
}
