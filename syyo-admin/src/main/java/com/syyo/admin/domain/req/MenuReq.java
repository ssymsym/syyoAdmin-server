package com.syyo.admin.domain.req;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * @Auther: wangzhong
 * @Date: 2019/9/21 17:23
 * @Description: 菜单请求实体类
 */
@Data
public class MenuReq  {

    private Integer menuId; //菜单id
    @NotNull(message = "menuName不能为空")
    private String menuName;//菜单名称
    private Integer pid;//父级id
    private String pids;//父级id数组，数据库存储

    private String perms;//权限标识
    private String path;//权限标识
    private String component;//权限标识
    private String componentName;//权限标识
    private Boolean hidden;//权限标识
    private Boolean cache;//权限标识
    private String icon;//权限标识
    private Integer type;//类型
    private Integer sort;//排序

    private List<Integer> pidList;//父级id列表，前端接收
}
