package com.syyo.admin.domain.req;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * @Auther: wangzhong
 * @Date: 2019/9/21 17:23
 * @Description: 角色请求实体类
 */
@Data
public class RoleReq {

    private Integer roleId; //角色id
    @NotNull(message = "roleName不能为空")
    private String roleName;//角色名称
    @NotNull(message = "roleKey不能为空")
    private String roleKey;//权限标识
    private Integer dataScope;//数据范围
    private String remark;//备注

    private List<Integer> menuIdList;//菜单id列表
    private List<Integer> deptList;//数据权限
}
