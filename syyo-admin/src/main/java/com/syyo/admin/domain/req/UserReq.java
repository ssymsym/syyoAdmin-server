package com.syyo.admin.domain.req;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Auther: wangzhong
 * @Date: 2019/9/30 17:20
 * @Description: 用户请求实体类
 */
@Data
public class UserReq {

    private Integer userId; //用户id
    @NotNull(message = "账号不能为空")
    private String userName;//用户账号
    private String nickName;//昵称
    @NotNull(message = "密码不能为空")
    private String passWord;//密码
    private String newPassWord;//新密码
    private String token;//token
    private String phone;//手机
    private String email;//邮箱
    private Integer sex;//性别
    private String avatar;
    private String remark;//备注
    private Integer status;//状态（1：启用，0：禁用）

    private List<Integer> roleIdList;//角色id







}
