package com.syyo.admin.domain.resp;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;


/**
 * @Auther: wangzhong
 * @Date: 2019/9/21 17:23
 * @Description: 菜单请求实体类
 */
@Data
public class MenuResp {

    private Integer menuId; //菜单id
    private String menuName;//菜单名称
    private Integer pid;//父级id
    private String perms;//权限标识

    private String path;//前端跳转路由
    private String component;//前端组件地址
    private String componentName;//前端组件name
    private Boolean hidden;//是否隐藏
    private Boolean cache;//是否缓存
    private String icon;//图标
    private Integer type;//类型
    private Integer sort;//排序
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private String remark;

    private List<Integer> pidList;//父级id列表
    private List<MenuResp> children;
}
