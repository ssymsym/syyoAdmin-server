package com.syyo.admin.domain.resp;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;


/**
 * @Auther: wangzhong
 * @Date: 2019/9/21 17:23
 * @Description: 角色请求实体类
 */
@Data
public class RoleResp {

    private Integer roleId; //角色id
    private String roleName;//角色名称
    private String roleKey;//权限标识
    private Integer dataScope;//数据范围
    private String remark;//备注
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

    private List<Integer> menuIdList;//菜单id列表
    private List<Integer> deptIdList;//部门id列表
}
