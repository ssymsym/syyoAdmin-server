package com.syyo.admin.domain.resp;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Auther: wangzhong
 * @Date: 2019/9/30 17:20
 * @Description: 用户请求实体类
 */
@Data
public class UserResp   {

    private Integer userId; //用户id
    private String userName;//用户账号
    private String nickName;//昵称
    private String phone;//手机
    private String email;//邮箱
    private Integer sex;//性别
    private String remark;//备注
    private Integer status;//状态（1：启用，0：禁用）
    private String avatar;
    private List<Integer> roleIdList;//角色id
    private List<Integer> deptIdList;//部门id
    private DeptResp dept;//部门对象
    private LocalDateTime createTime;





}
