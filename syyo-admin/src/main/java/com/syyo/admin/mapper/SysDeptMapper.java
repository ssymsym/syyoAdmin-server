package com.syyo.admin.mapper;

import com.syyo.admin.domain.entity.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.syyo.admin.domain.resp.DeptResp;

import java.util.List;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2020-08-20
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    List<Integer> getChildren(Integer deptId);

    DeptResp getDeptByUserId(Integer userId);
}
