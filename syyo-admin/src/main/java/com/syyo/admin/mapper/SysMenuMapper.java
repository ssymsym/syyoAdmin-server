package com.syyo.admin.mapper;

import com.syyo.admin.domain.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    Set<SysMenu> getMenuByRoleId(Integer roleId);

    List<Integer> findZi(Integer menuId);

}
