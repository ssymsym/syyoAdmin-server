package com.syyo.admin.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysRoleDeptMapper {

    Integer addAll(@Param("roleId")Integer roleId, @Param("deptList")List<Integer> deptList);

    Integer del(@Param("roleId")Integer roleId);

    List<Integer> getDeptById(Integer roleId);
}
