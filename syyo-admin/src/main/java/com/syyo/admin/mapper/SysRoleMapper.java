package com.syyo.admin.mapper;

import com.syyo.admin.domain.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    Set<SysRole> findByUsersId(Integer userId);

    List<SysRole> getRoleByUserName(String username);
}
