package com.syyo.admin.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysRoleMenuMapper{

    Integer add(@Param("roleId")Integer roleId, @Param("menuId")Integer menuId);

    Integer addAll(@Param("roleId")Integer roleId, @Param("menuIds")List<Integer> menuIds);

    Integer del(@Param("roleId")Integer roleId);

    List<Integer> getMenuId(@Param("roleId")Integer roleId);
}
