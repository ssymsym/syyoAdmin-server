package com.syyo.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.syyo.admin.domain.entity.SysUser;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<Integer> getRoleIdByUserName(String username);

    Integer getDeptByName(String username);
}
