package com.syyo.admin.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysUserRoleMapper{

    Integer add(@Param("userId")Integer userId,@Param("roleId")Integer roleId);

    Integer addAll(@Param("userId")Integer userId,@Param("roleIds")List<Integer> roleIds);

    Integer del(@Param("userId")Integer userId);

    List<Integer> findRole(Integer userId);
}
