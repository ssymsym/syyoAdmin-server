package com.syyo.admin.service;

import com.syyo.common.domain.FileUpload;
import com.syyo.common.domain.ResultVo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/18 10:17
 * @Description:
 */
public interface FileService {

    /**
     * 上传文件
     * @param file
     * @return
     */
    ResultVo<FileUpload> uploadFile(MultipartFile file);

}
