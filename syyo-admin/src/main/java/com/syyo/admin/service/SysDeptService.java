package com.syyo.admin.service;

import com.syyo.admin.domain.entity.SysDept;
import com.syyo.admin.domain.req.DeptReq;
import com.baomidou.mybatisplus.extension.service.IService;
import com.syyo.common.domain.ResultVo;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author wang
 * @since 2020-08-20
 */
public interface SysDeptService extends IService<SysDept> {

    ResultVo add(DeptReq req);

    ResultVo del(Integer id);

    ResultVo edit(DeptReq req);

    ResultVo findOne(Integer id);

    ResultVo tree();
}
