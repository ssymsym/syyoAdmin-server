package com.syyo.admin.service;

import com.syyo.admin.domain.req.MenuReq;
import com.syyo.admin.domain.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.syyo.common.domain.ResultVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 菜单新增
     * @param req
     * @return
     */
    ResultVo add(MenuReq req);

    /**
     * 菜单删除
     * @param id
     * @return
     */
    ResultVo del(Integer id);

    /**
     * 菜单编辑
     * @param req
     * @return
     */
    ResultVo edit(MenuReq req);

    /**
     * 菜单详情
     * @param
     * @return
     */
    ResultVo findOne(Integer userId);

    /**
     * 菜单列表（树形结构）
     * @param
     * @return
     */
    ResultVo tree();

    /**
     * 菜单列表（前端左侧导航用的）
     * @param
     * @return
     */
    ResultVo buildMenu();
}
