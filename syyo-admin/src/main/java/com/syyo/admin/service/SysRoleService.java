package com.syyo.admin.service;

import com.syyo.admin.domain.req.RoleReq;
import com.syyo.admin.domain.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.syyo.common.domain.ResultVo;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysRoleService extends IService<SysRole> {

    ResultVo add(RoleReq req);

    ResultVo del(Integer id);

    ResultVo edit(RoleReq req);

    ResultVo findOne(Integer id);

    ResultVo findAll(Integer pageNum, Integer pageSize, RoleReq req);

    Collection<GrantedAuthority> findByUsersId(Integer userId);

    ResultVo auth(RoleReq req);
}
