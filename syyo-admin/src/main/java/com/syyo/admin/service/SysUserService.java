package com.syyo.admin.service;


import com.syyo.admin.domain.req.UserReq;
import com.syyo.admin.domain.entity.SysUser;
import com.syyo.admin.domain.excel.UserExport;
import com.baomidou.mybatisplus.extension.service.IService;
import com.syyo.common.domain.ResultVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
public interface SysUserService extends IService<SysUser> {

    ResultVo add(UserReq req,Integer deptId);

    ResultVo del(Integer id,Integer deptId);

    ResultVo edit(UserReq req,Integer deptId);

    ResultVo findOne(Integer id);

    ResultVo findAll(Integer pageNum, Integer pageSize, UserReq req,Integer deptId);

    ResultVo importUser(MultipartFile file) throws IOException;

    ResultVo exportUser(UserExport userExport, HttpServletResponse response);

    ResultVo editPwd(UserReq req);

    ResultVo editAvatar(MultipartFile file);
}
