package com.syyo.admin.service.impl;

import com.syyo.admin.domain.entity.SysDept;
import com.syyo.admin.domain.req.DeptReq;
import com.syyo.common.domain.ResultVo;
import com.syyo.common.enums.ResultEnum;
import com.syyo.common.exception.SysException;
import com.syyo.common.utils.MyStringUtils;
import com.syyo.common.utils.ResultUtils;

import com.syyo.admin.mapper.SysDeptMapper;
import com.syyo.admin.service.SysDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.syyo.admin.utils.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2020-08-20
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Autowired
    private SysDeptMapper deptMapper;

    @Override
    @Transactional
    public ResultVo add(DeptReq req) {
        SysDept sysDept = new SysDept();
        List<Integer> pidList = req.getPidList();
        Integer pid = MyStringUtils.getPid(pidList);
        String pids = MyStringUtils.getPids(pidList);
        Integer rank = MyStringUtils.getRank(pidList);
        sysDept.setPid(pid);
        sysDept.setPids(pids);
        sysDept.setRank(rank);
        sysDept.setDeptName(req.getDeptName());
        LocalDateTime now = LocalDateTime.now();
        sysDept.setUpdateTime(now);
        sysDept.setCreateTime(now);
        int insert = deptMapper.insert(sysDept);
        if (insert != 1){
            throw new SysException(ResultEnum.E_80031.getCode(),ResultEnum.E_80031.getMessage());
        }
        return ResultUtils.ok(insert);
    }

    @Override
    @Transactional
    public ResultVo del(Integer id) {
        int insert = deptMapper.deleteById(id);
        if (insert != 1){
            throw new SysException(ResultEnum.E_80032.getCode(),ResultEnum.E_80032.getMessage());
        }
        return ResultUtils.ok(insert);
    }

    @Override
    @Transactional
    public ResultVo edit(DeptReq req) {
        SysDept sysDept = new SysDept();
        List<Integer> pidList = req.getPidList();
        Integer pid = MyStringUtils.getPid(pidList);
        String pids = MyStringUtils.getPids(pidList);
        Integer rank = MyStringUtils.getRank(pidList);
        sysDept.setDeptId(req.getDeptId());
        sysDept.setPid(pid);
        sysDept.setPids(pids);
        sysDept.setRank(rank);
        sysDept.setDeptName(req.getDeptName());
        LocalDateTime now = LocalDateTime.now();
        sysDept.setCreateTime(now);
        int insert = deptMapper.updateById(sysDept);
        if (insert != 1){
            throw new SysException(ResultEnum.E_80033.getCode(),ResultEnum.E_80033.getMessage());
        }
        return ResultUtils.ok(insert);
    }

    @Override
    public ResultVo findOne(Integer id) {
        SysDept sysDept = deptMapper.selectById(id);
        return ResultUtils.ok(sysDept);
    }

    @Override
    public ResultVo tree() {
        List<SysDept> list = deptMapper.selectList(null);
        List<SysDept> menuResps = TreeUtils.listToDeptTree(list);
        return ResultUtils.ok(menuResps);
    }
}
