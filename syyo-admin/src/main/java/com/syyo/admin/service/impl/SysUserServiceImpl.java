package com.syyo.admin.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.syyo.admin.common.listener.UploadDataListener;
import com.syyo.admin.domain.entity.SysUser;
import com.syyo.admin.domain.excel.UserExport;
import com.syyo.admin.domain.excel.UserImport;
import com.syyo.admin.domain.req.UserReq;
import com.syyo.admin.domain.resp.DeptResp;
import com.syyo.admin.domain.resp.UserResp;
import com.syyo.admin.mapper.SysDeptMapper;
import com.syyo.admin.mapper.SysUserMapper;
import com.syyo.admin.mapper.SysUserRoleMapper;
import com.syyo.admin.service.SysUserService;
import com.syyo.common.constant.SettingsConstant;
import com.syyo.common.domain.ResultVo;
import com.syyo.common.enums.ResultEnum;
import com.syyo.common.exception.SysException;
import com.syyo.common.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wang
 * @since 2020-07-17
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Autowired
    private SettingsConstant settingsConstant;

    @Autowired
    private SysDeptMapper deptMapper;

    @Override
    @Transactional
    public ResultVo add(UserReq req,Integer deptId) {
        SysUser sysUser = new SysUser();
        sysUser.setUserName(req.getUserName());
        sysUser.setNickName(req.getNickName());
        sysUser.setEmail(req.getEmail());
        sysUser.setSex(req.getSex());
        sysUser.setRemark(req.getRemark());
        sysUser.setStatus(1);//默认状态为可用
        sysUser.setDeptId(deptId);

        String encode = passwordEncoder.encode(req.getPassWord());
        sysUser.setPassWord(encode);
        LocalDateTime now = LocalDateTime.now();
        sysUser.setCreateTime(now);
        sysUser.setUpdateTime(now);
        //添加用户
        int insert = userMapper.insert(sysUser);
        if (insert != 1){
            throw new SysException(ResultEnum.E_80001.getCode(),ResultEnum.E_80001.getMessage());
        }

        //添加角色菜单中间表
        List<Integer> roleIdList = req.getRoleIdList();
        if (MyListUtils.isNotEmpty(roleIdList)){
            // 只有admin用户才能设置admin角色
            if (roleIdList.contains(1)){
                if (!"admin".equals(SecurityUtils.getUsername())){
                    throw new SysException(ResultEnum.E_90008.getCode(),ResultEnum.E_90008.getMessage());
                }
            }
            Integer userId = sysUser.getUserId();
            Integer add = userRoleMapper.addAll(userId, roleIdList);
            if (add != roleIdList.size()){
                throw new SysException(ResultEnum.E_80011.getCode(),ResultEnum.E_80011.getMessage());
            }
        }
        return ResultUtils.ok(insert);

    }

    @Override
    @Transactional
    public ResultVo del(Integer id,Integer deptId) {
        // admin用户不能删除
        if (id == 1){
            throw new SysException(ResultEnum.E_90007.getCode(),ResultEnum.E_90007.getMessage());
        }

        int insert = userMapper.deleteById(id);
        if (insert != 1){
            throw new SysException(ResultEnum.E_80002.getCode(),ResultEnum.E_80002.getMessage());
        }
        return ResultUtils.ok(insert);
    }

    @Override
    @Transactional
    public ResultVo edit(UserReq req,Integer deptId) {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(req.getUserId());
        sysUser.setNickName(req.getNickName());
        sysUser.setDeptId(deptId);
        sysUser.setEmail(req.getEmail());
        sysUser.setSex(req.getSex());
        sysUser.setRemark(req.getRemark());
        sysUser.setStatus(req.getStatus());
        LocalDateTime now = LocalDateTime.now();
        sysUser.setUpdateTime(now);
        int insert = userMapper.updateById(sysUser);
        if (insert != 1){
            throw new SysException(ResultEnum.E_80003.getCode(),ResultEnum.E_80003.getMessage());
        }

        //添加角色菜单中间表
        List<Integer> roleIdList = req.getRoleIdList();
        if (MyListUtils.isNotEmpty(roleIdList)){
            // 只有admin用户才能设置admin角色
            if (roleIdList.contains(1)){
                String username = SecurityUtils.getUsername();
                if (!"admin".equals(SecurityUtils.getUsername())){
                    throw new SysException(ResultEnum.E_90008.getCode(),ResultEnum.E_90008.getMessage());
                }
            }
            Integer userId = sysUser.getUserId();
            //删除之前用户角色中间表的数据
            Integer del = userRoleMapper.del(userId);

            //添加用户角色中间表
            Integer add = userRoleMapper.addAll(userId, roleIdList);
            if (add != roleIdList.size()){
                throw new SysException(ResultEnum.E_80011.getCode(),ResultEnum.E_80011.getMessage());
            }
        }
        return ResultUtils.ok(insert);
    }

    @Override
    public ResultVo findOne(Integer id) {
        SysUser sysUser = userMapper.selectById(id);
        // 关联角色id
        List<Integer> list = userRoleMapper.findRole(id);
        UserResp userResp = new UserResp();
        userResp.setUserId(sysUser.getUserId());
        userResp.setUserName(sysUser.getUserName());
        userResp.setNickName(sysUser.getNickName());
        userResp.setSex(sysUser.getSex());
        userResp.setEmail(sysUser.getEmail());
        userResp.setRemark(sysUser.getRemark());
        userResp.setAvatar(sysUser.getAvatar());
        userResp.setStatus(sysUser.getStatus());
        userResp.setRoleIdList(list);
        // 查询用户关联的部门
        DeptResp dept = deptMapper.getDeptByUserId(id);
        List<Integer> pidList = new ArrayList<>();
        String[] splits = dept.getPids().split(",");
        for (String split : splits) {
            if (!"0".equals(split)){
                pidList.add(Integer.parseInt(split));
            }
        }
        pidList.add(dept.getDeptId());
        userResp.setDept(dept);
        userResp.setDeptIdList(pidList);
        return ResultUtils.ok(userResp);
    }

    @Override
    public ResultVo findAll(Integer pageNum, Integer pageSize, UserReq req,Integer deptId) {
        Page<SysUser> teacherPage = new Page<SysUser>(pageNum,pageSize);
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        String nickName = req.getNickName();
        //判断条件值是否为空，如果不为空拼接条件
        if(MyStringUtils.isNotEmpty(nickName)) {
            //构建条件
            wrapper.like("nick_name",nickName);
        }
        wrapper.eq("dept_id",deptId);
        IPage<SysUser> list = userMapper.selectPage(teacherPage, wrapper);
        List<UserResp> userResps = new ArrayList<>();
        for (SysUser user : list.getRecords()) {
            String userName = user.getUserName();
            DeptResp depts = deptMapper.getDeptByUserId(user.getUserId());
            UserResp userResp = new UserResp();
            userResp.setUserId(user.getUserId());
            userResp.setAvatar(user.getAvatar());
            userResp.setEmail(user.getEmail());
            userResp.setNickName(user.getNickName());
            userResp.setRemark(user.getRemark());
            userResp.setUserName(userName);
            userResp.setPhone(user.getPhone());
            userResp.setSex(user.getSex());
            userResp.setStatus(user.getStatus());
            userResp.setDept(depts);
            userResp.setCreateTime(user.getCreateTime());
            userResps.add(userResp);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("current",list.getCurrent());
        map.put("pages",list.getPages());
        map.put("records",userResps);
        map.put("size",list.getSize());
        map.put("total",list.getTotal());
        return ResultUtils.ok(map);
    }

    @Override
    @Transactional
    public ResultVo importUser(MultipartFile file) throws IOException {
        // 读取到的文件
        List<UserImport> importEmps = EasyExcel.read(file.getInputStream(), UserImport.class, new UploadDataListener(userMapper)).sheet().doReadSync();
        //  导入数据库
        int count = importEmps.size();
        int countok = importEmps.size();
        Map<String, Object> map = new HashMap<>();
        map.put("count",count);
        map.put("countOk",countok);
        return ResultUtils.ok(map);
    }

    @Override
    public ResultVo exportUser(UserExport userExport, HttpServletResponse response){
        List<SysUser> sysUsers = userMapper.selectList(null);
        List<UserExport> list = new ArrayList<>();
        for (SysUser sysUser : sysUsers) {
            UserExport userExport1 = new UserExport();
            userExport1.setUserName(sysUser.getUserName());
            userExport1.setNickName(sysUser.getNickName());
            userExport1.setEmail(sysUser.getEmail());
            userExport1.setPhone(sysUser.getPhone());
            list.add(userExport1);
        }
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        String dateStr = "user_" + MyDateUtils.getDateStr();
        response.setHeader("Content-disposition", "attachment;filename=" + dateStr + ".xlsx");
        // 这里需要设置不关闭流
        try {
            EasyExcel.write(response.getOutputStream(), UserExport.class)
                    .autoCloseStream(Boolean.FALSE).sheet("sheet").doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Transactional
    public ResultVo editPwd(UserReq req) {
        // 获取用户名
        SysUser sysUser = getUserByName();
        String oldPwdRsa = req.getPassWord();
        String newPwdRsa = req.getNewPassWord();
        // RSA 解密
        String oldPwd = MyRsaUtils.decrypt(oldPwdRsa, settingsConstant.getPrivateKey());
        String newPwd = MyRsaUtils.decrypt(newPwdRsa, settingsConstant.getPrivateKey());

        if (!passwordEncoder.matches(oldPwd, sysUser.getPassWord())){
            throw new SysException(ResultEnum.E_80008.getCode(),ResultEnum.E_80008.getMessage());
        }
        if (passwordEncoder.matches(newPwd, sysUser.getPassWord())){
            throw new SysException(ResultEnum.E_80009.getCode(),ResultEnum.E_80009.getMessage());
        }
        SysUser newsysUser = new SysUser();
        newsysUser.setUserId(sysUser.getUserId());
        newsysUser.setPassWord(passwordEncoder.encode(newPwd));
        int insert = userMapper.updateById(newsysUser);
        if (insert != 1){
            throw new SysException(ResultEnum.E_80003.getCode(),ResultEnum.E_80003.getMessage());
        }
        return ResultUtils.ok(insert);
    }

    private SysUser getUserByName() {
        String username = SecurityUtils.getUsername();
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name", username);
        SysUser sysUser = userMapper.selectOne(wrapper);
        if(sysUser == null){
            throw new SysException(ResultEnum.E_80005.getCode(),ResultEnum.E_80005.getMessage());
        }
        return sysUser;
    }

    @Override
    @Transactional
    public ResultVo editAvatar(MultipartFile multipartFile) {
        // 获取用户名
        SysUser sysUser = getUserByName();
        File file = MyFileUtil.upload(multipartFile, settingsConstant.getImagePath());
        assert file != null;
        SysUser newsysUser = new SysUser();
        newsysUser.setUserId(sysUser.getUserId());
        newsysUser.setAvatar(file.getPath());
        int insert = userMapper.updateById(newsysUser);
        if (insert != 1){
            throw new SysException(ResultEnum.E_80003.getCode(),ResultEnum.E_80003.getMessage());
        }
        return ResultUtils.ok(insert);
    }
}
