package com.syyo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * @Auther: wangzhong
 * @Date: 2020/7/17 16:31
 * @Description:
 */
public class Ceshi {

    public static void main(String[] args) {
//        test01();
//        test02();
        test03();
    }

    private static void test03() {

        ArrayList<Integer> pidList = new ArrayList<>();

//        pidList.add(1);
//        pidList.add(2);
//        pidList.add(3);
//        pidList.add(4);
//        pidList.add(8);
        Integer type = pidList.size() ==0 ? 0:pidList.size();
        System.out.println(type);
    }

    private static void test02() {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.now();
        String timeStr = df.format(localDateTime);

        System.out.println(timeStr);

        LocalDateTime parse = LocalDateTime.parse(timeStr, df);
        System.out.println(parse);
    }


    private static void test01() {
        LocalDateTime now = LocalDateTime.now();
        LocalDate now1 = LocalDate.now();
        LocalTime now2 = LocalTime.now();

        LocalDate endOfFeb = LocalDate.parse("2018-12-10");
        System.out.println(now);
        System.out.println(now1);
        System.out.println(now2);
        System.out.println(endOfFeb);
    }

}
