package com.syyo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.Date;


//@RunWith(SpringRunner.class)
//@SpringBootTest
public class JWTTest {

    private static String privateKey = "MIICeQIBADANBgkqhkiG9w0BAQEFAASCAmMwggJfAgEAAoGBALzltUfm6CWJws7L\n" +
            "Fp3cN2Gm8pje6kiLQ/Z0naNH2GcBGrT95Mi62DMX7joICw0SoWau7Lz48ff5ldUE\n" +
            "khN1AafhHKvoUfLv1RE3A5hNFJ+q/nv7gMkVkLiW+fbi8oOIE0z6Q+u6VCmx8zFu\n" +
            "WWOM+wtMLcBEj8/6OG7OcLI7nDbNAgMBAAECgYEAl2+gkXAwlEeJAbgW/Z9aVtM8\n" +
            "kgnD0Ty005RR/GDQwGNGQet5yuLgCLYaqjNgiK5y7ps195msgpx43jm30hlvpQDk\n" +
            "NrJzGqqqszc2ZZvvIAJCTbTth5Rkqdd/3LjsOqSF0kZ0zti/FoBxnKqaCnQ11GVT\n" +
            "M+e/1HDc22p3H8Bz6ZECQQDxBkH3/KnGV1UhP+4DHfsAwrRXsmROF04EnGZFEr4L\n" +
            "hemkG11j4W/0iE94W9EssHnoeSJNz3pCrDrY7EWVCvkjAkEAyKJQfdI177dlbG2X\n" +
            "/vtwIgbYbKdLX91egbZ6ECOKO8j5fEkAF71bKFus1JJVSKYM9arduHF/SRyLPPlz\n" +
            "VncnTwJBAJkWWmxCpSr/aWjXDDwanMTc5mMVQbSABOPU2vhgcSksgzvizayVEeEF\n" +
            "e0R9PRzfcm8AEZPOhl82uaHyYDfMYnUCQQCDaoYMlBdOKDNf3T01AB8HurIiIUZm\n" +
            "yYowroSvc4gE9vQrq2lLI3XFLp87MYp5JoLQm5XVTUpRakftrXsVxMBTAkEA03Fb\n" +
            "SYirqqC/qELL3dT2kM+mWmX+3UIXVWDXfjqM0wvvK77tUhl6DFnk3IgS9wv952Hy\n" +
            "0DXcL1hXJmn4Wh9IEQ==";
    private static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC85bVH5uglicLOyxad3DdhpvKY\n" +
            "3upIi0P2dJ2jR9hnARq0/eTIutgzF+46CAsNEqFmruy8+PH3+ZXVBJITdQGn4Ryr\n" +
            "6FHy79URNwOYTRSfqv57+4DJFZC4lvn24vKDiBNM+kPrulQpsfMxblljjPsLTC3A\n" +
            "RI/P+jhuznCyO5w2zQIDAQAB";

    public static void main(String[] args) {
        String encode = encode();
        System.out.println("=========加密完成==========");
        System.out.println(encode);

        System.out.println("=========开始解密==========");
        String decode = decode(encode);
        System.out.println(decode);
        System.out.println("=========解密完成==========");
    }


    public static String encode()   {
        //过期时间
        long now = (new Date()).getTime();
        Date validity = new Date(now + 7200000);
        byte[] keyBytes = privateKey.getBytes();
        Key key = Keys.hmacShaKeyFor(keyBytes);
        //加密
        String compact = Jwts.builder()
                .setSubject("user")//用户名
                .claim("auth", "user:add")//权限字符串
                .signWith(key, SignatureAlgorithm.HS256)//使用加密
                .setExpiration(validity)//过期时间
                .compact();

        return compact;
    }


    public static String decode(String token)   {
        byte[] keyBytes = privateKey.getBytes();
        Key key = Keys.hmacShaKeyFor(keyBytes);
        // 解密
        Claims claims = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody();
        return claims.toString();
    }
}
