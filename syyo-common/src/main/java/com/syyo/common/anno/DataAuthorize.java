package com.syyo.common.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据权限注解
 * 1：全部，2：自定义，3：本部门及以下部门，4：本部门
 * 多个角色的数据权限，取最大级的那个
 * 在需要数据权限的接口上加上注解，部门id放在参数最后一位，样例：@PathVariable("deptId")Integer deptId
 */
@Target(ElementType.METHOD)//这个注解是应用在方法上
@Retention(RetentionPolicy.RUNTIME)
public @interface DataAuthorize {
}
