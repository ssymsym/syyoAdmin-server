package com.syyo.common.constant;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: wangzhong
 * @Date: 2019/10/21 16:17
 * @Description: 系统配置类
 */
@Data
@Configuration
public class SettingsConstant {

    /*    ========= 分页 ===========  */
    @Value("${settings.page.num}")
    private String pageNum; //当前页
    @Value("${settings.page.size}")
    private String pageSize; //每页条数
    @Value("${settings.page.range}")
    private String pageRange; //范围

    /*    ========= 文件 ===========  */
    @Value("${settings.file.image}")
    private String imagePath; //图片路劲
    @Value("${settings.file.documents}")
    private String documentsPath; //文档路劲
    @Value("${settings.file.music}")
    private String musicPath; //音乐路劲
    @Value("${settings.file.video}")
    private String videoPath; //视频路劲
    @Value("${settings.file.other}")
    private String otherPath; //其他路劲

    /*    ========= security开关 ===========  */
    @Value("${settings.security.switch}")
    private Boolean securitySwitch; //securitySwitch 开关

    /*    ========= swagger开关 ===========  */
    @Value("${settings.swagger.switch}")
    private Boolean swaggerSwitch; //swaggerSwitch 开关

    /*    ========= 代码生成器开关 ===========  */
    @Value("${settings.generator.switch}")
    private Boolean generatorSwitch; //代码生成器 开关

    /*    ========= RSA 私钥 公钥===========  */
    @Value("${settings.rsa.privatekey}")
    private String privateKey; //私钥
    @Value("${settings.rsa.publicKey}")
    private String publicKey; //公钥

    /*    ========= 验证码 ===========  */
    @Value("${settings.loginCode.expiration}")
    private Integer loginCodeExpiration; //登录验证码超时时间

    /*    ========= 域名 ===========  */
    @Value("${settings.domain.image}")
    private String domainImage; //图片服务器域名

    /*    ========= 是否是演示环境 ===========  */
    @Value("${settings.demo.switch}")
    private Boolean isDemo; // 开关
}
