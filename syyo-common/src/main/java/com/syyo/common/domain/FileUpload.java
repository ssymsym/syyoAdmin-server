package com.syyo.common.domain;

import lombok.Data;

/**
 * @Auther: wangzhong
 * @Date: 2019/10/21 16:14
 * @Description: 文件对象实体类
 */
@Data
public class FileUpload {

    private String url;
    private String fileName;
    private String fileSize;
    private String fileType;
}
