package com.syyo.common.domain;

import lombok.Data;

/**
 * @Auther: wangzhong
 * @Date: 2019/6/29 09:50
 * @Description: 统一返回实体类
 */
@Data
public class ResultVo<T> {

    private Integer code; //状态码
    private String message; //提示信息
    private T data; //响应数据

}