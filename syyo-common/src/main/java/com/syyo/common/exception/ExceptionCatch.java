package com.syyo.common.exception;


import com.syyo.common.domain.ResultVo;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 统一捕获自定义异常类
 */
@RestControllerAdvice//控制器增强
public class ExceptionCatch {

    /**
     * 处理所有不可知的异常
     */
//    @ExceptionHandler(Throwable.class)
//    public ResultVo handleException(Throwable e){
//        // 打印堆栈信息
//        ResultVo<Object> resultVo = new ResultVo<>();
//        resultVo.setCode(9999);
//        resultVo.setMessage(e.getMessage());
//        return resultVo;
//    }

    /**
     * BadCredentialsException
     */
//    @ExceptionHandler(BadCredentialsException.class)
//    public ResultVo badCredentialsException(BadCredentialsException e){
//        // 打印堆栈信息
//        String message = "坏的凭证".equals(e.getMessage()) ? "用户名或密码不正确" : e.getMessage();
//        ResultVo<Object> resultVo = new ResultVo<>();
//        resultVo.setCode(8000);
//        resultVo.setMessage(message);
//        return resultVo;
//    }

    /**
     * UsernameNotFoundException
     */
//    @ExceptionHandler(UsernameNotFoundException.class)
//    public ResultVo badCredentialsException2(UsernameNotFoundException e){
//        // 打印堆栈信息
//        String message = "坏的凭证".equals(e.getMessage()) ? "用户名或密码不正确" : e.getMessage();
//        ResultVo<Object> resultVo = new ResultVo<>();
//        resultVo.setCode(8002);
//        resultVo.setMessage(message);
//        return resultVo;
//    }

    /**
     * InternalAuthenticationServiceException
     */
//    @ExceptionHandler(InternalAuthenticationServiceException.class)
//    public ResultVo badCredentialsException(InternalAuthenticationServiceException e){
//        // 打印堆栈信息
//        ResultVo<Object> resultVo = new ResultVo<>();
//        resultVo.setCode(8001);
//        resultVo.setMessage(e.getMessage());
//        return resultVo;
//    }

    /**
     * SysException
     */
    @ExceptionHandler(SysException.class)
    public ResultVo customException(SysException sysException){
        ResultVo<Object> resultVo = new ResultVo<>();
        resultVo.setCode(sysException.getCode());
        resultVo.setMessage(sysException.getMessage());
        return resultVo;
    }



}
