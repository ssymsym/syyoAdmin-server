package com.syyo.common.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Auther: wangzhong
 * @Date: 2019/6/28 16:47
 * @Description: 异常
 */
@Data
@AllArgsConstructor
public class SysException extends RuntimeException {

    private Integer code;
    private String message;

}
