package com.syyo.common.utils;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import java.util.Map;
import java.util.Set;

/**
 * @Auther: wangzhong
 * @Date: 2020/4/10 11:41
 * @Description: 校验路劲
 */
public class CheckUriUtils {

    public static boolean checkUri(String uri,Map<RequestMappingInfo, HandlerMethod> map){
        boolean uriBoolean = false;
        for (RequestMappingInfo info : map.keySet()) {
            // 获取url的Set集合，一个方法可能对应多个url
            Set<String> patterns = info.getPatternsCondition().getPatterns();
            for (String url : patterns) {
                // 把结果存入静态变量中程序运行一次次方法之后就不用再次请求次方法
                if (uri.equals(url) || "/error".equals(uri)){
                    uriBoolean = true;
                    break;
                }
            }
        }
        return uriBoolean;
    }
}
