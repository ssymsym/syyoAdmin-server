package com.syyo.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * @Auther: wangzhong
 * @Date: 2019/6/29 14:12
 * @Description:
 */
public class MyDateUtils {

    private static final String DATE_FORMAT_8 = "yyyyMMdd";
    private static final String DATE_FORMAT_10 = "yyyy-MM-dd";
    private static final String DATE_FORMAT_10_FORWARD_SLASH = "yyyy/MM/dd";
    private static final String DATE_FORMAT_14 = "yyyyMMddHHmmss";
    private static final String DATE_FORMAT_17 = "yyyyMMdd HH:mm:ss";
    private static final String DATE_FORMAT_19 = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT_19_FORWARD_SLASH = "yyyy/MM/dd HH:mm:ss";


    /**
     * 获取当前时间
     * @return
     */
    public static Date getDate(){
        return  new Date();
    }

    /**
     * LocalDateTime 转时间字符串
     * @param localDateTime
     * @return
     */
    public static String localDateTimeToTimeStr(LocalDateTime localDateTime){
        DateTimeFormatter df = DateTimeFormatter.ofPattern(DATE_FORMAT_19);
        return df.format(localDateTime);
    }

    /**
     * 时间字符串 转 LocalDateTime
     * @param
     * @return
     */
    public static LocalDateTime TimeStrTolocalDateTime(String timeStr){
        DateTimeFormatter df = DateTimeFormatter.ofPattern(DATE_FORMAT_19);
        return LocalDateTime.parse(timeStr, df);
    }

    /**
     *  拼接时间  2020/01/01
     * @return
     */
    public static String getDateFileName() {
        LocalDateTime now = LocalDateTime.now();
        String month = "";
        int year = now.getYear();
        int monthValue = now.getMonthValue();
        int day = now.getDayOfMonth();
        if (monthValue < 10) {
            month = "0" + monthValue;
        } else {
            month = monthValue + "";
        }
        return year + "/" + month + "/" + day + "/";
    }

    /**
     * 时间转成 字符串  20200101121200
     * @return
     */
    public static String getDateStr(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_14);
        return sdf.format(date);
    }

    /**
     * 时间戳转换时间
     * @param timeStamp
     * @return
     */
    public static Date timeStampToDate(Long timeStamp){
        return  new Date(timeStamp);
    }

    /**
     * 获取时间字符串
     * @return
     */
    public static long getTimeStamp(){
        return System.currentTimeMillis();//1563790644819
    }

    /**
     * 将时间格式时间转换为字符串 yyyy-MM-dd HH:mm:ss
     *
     * @param dateDate
     * @return
     */
    public static String dateToTimeStr(Date dateDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_19);
        return sdf.format(dateDate);
    }

    /**
     * 将字符串 yyyy-MM-dd HH:mm:ss 转换为时间格式时间 date
     *
     * @param
     * @return
     */
    public static Date timeStrToDate(String timeStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_19);
        try {
            return sdf.parse(timeStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将时间格式时间转换为字符串 yyyy-MM-dd
     *
     * @param dateDate
     * @return
     */
    public static String dateToDateStr(Date dateDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_10);
        return sdf.format(dateDate);
    }

    public static Date dateStrToDate(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_10);
        return sdf.parse(dateStr);
    }

    public static boolean isWeekend(String dateStr) throws ParseException {
        Date date = dateStrToDate(dateStr);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            return true;
        } else{
            return false;
        }
    }


    /**
     *  拼接时间  2020-01-01 12:00:00
     * @return
     */
    public static String getDateStr2() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter df = DateTimeFormatter.ofPattern(DATE_FORMAT_19);
        return df.format(now);
    }

    /**
     *  计算时间差
     * @param startDateLong
     * @param endDateLong
     * @return
     */
    public static String countTimeDifference(Long startDateLong, Long endDateLong) {

        // 考勤时间 上午 2020-05-15 09:00:00 - 2020-05-15 12:00:00
        // 考勤时间 下午 2020-05-15 14:00:00 - 2020-05-15 18:00:00

        long nh = 1000 * 60 * 60;//时
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDateLong - startDateLong;

        // 计算差多少小时
        long hour = diff  / nh;
        return  hour + "小时";
    }

    /**
     * 判断当前时间是否在一段时间范围内
     * @param nowDate
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    private static boolean isRang(String nowDate, String startDate, String endDate) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date now = format.parse(nowDate);
        Date start = format.parse(startDate);
        Date end = format.parse(endDate);
        long nowTime = now.getTime();
        long startTime = start.getTime();
        long endTime = end.getTime();
        return nowTime >= startTime && nowTime <= endTime;
    }

    /**
     * 判断当前时间是否在一段时间范围内
     * @param
     * @param
     * @param
     * @return
     * @throws Exception
     */
    public static boolean compareTime(String nowTime, String workTime) {
        if (!workTime.contains(":") || !nowTime.contains(":")) {
            System.out.println("格式不正确");
        }else {
            String[] array1 = workTime.split(":");
            int work = Integer.valueOf(array1[0])*3600+Integer.valueOf(array1[1])*60+Integer.valueOf(array1[2]);
            String[] array2 = nowTime.split(":");
            int now = Integer.valueOf(array2[0])*3600+Integer.valueOf(array2[1])*60+Integer.valueOf(array2[2]);
            if (now < work){
                System.out.println("当前时间小于上班时间\n" +nowTime + " < " + workTime);
                return true;
            }else {
                System.out.println("当前时间大于上班时间\n" +nowTime + " > " + workTime);
                return false;
            }
        }
        return true;
    }
}
