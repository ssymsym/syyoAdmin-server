package com.syyo.common.utils;

import java.util.List;
import java.util.Set;

/**
 * @Auther: wangzhong
 * @Date: 2019/10/16 18:35
 * @Description:
 */
public class MyListUtils {

    /**
     * 校验集合是否为空
     * @param list
     * @return
     */
    public static boolean isEmpty(List list){
        if (list != null && list.size() > 0){
            return false;
        }else {
            return true;
        }
    }

    public static boolean isNotEmpty(List list){
        if (list != null && list.size() > 0){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 校验集合是否为空
     * @param set
     * @return
     */
    public static boolean setIsNull(Set set){
        if (set != null && set.size() > 0){
            return false;
        }else {
            return true;
        }
    }

    public static boolean setIsNotNull(Set set){
        if (set != null && set.size() > 0){
            return true;
        }else {
            return false;
        }
    }
}
