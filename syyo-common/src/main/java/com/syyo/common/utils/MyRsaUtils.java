package com.syyo.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

@Slf4j
public class MyRsaUtils {

    /**
     * RSA公钥加密
     * @param str 加密前的字符串
     * @param publicKey 私钥
     * @return 密文
     * @throws Exception
     *             解密过程中的异常信息
     */
    public static String encrypt( String str, String publicKey ) {
        //base64编码的公钥
        String outStr = "";
        byte[] decoded = Base64.decodeBase64(publicKey);
        RSAPublicKey pubKey = null;
        try {
            pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
            //RSA加密
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            outStr = Base64.encodeBase64String(cipher.doFinal(str.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("RSA 加密错误");
        }
        return outStr;
    }

    /**
     * RSA私钥解密
     * @param str 加密后的字符串
     * @param privateKey 私钥
     * @return 明文
     * @throws Exception
     *             解密过程中的异常信息
     */
    public static String decrypt(String str, String privateKey){
        //64位解码加密后的字符串
        String outStr = "";
        byte[] inputByte = new byte[0];
        try {
            inputByte = Base64.decodeBase64(str.getBytes("UTF-8"));
            //base64编码的私钥
            byte[] decoded = Base64.decodeBase64(privateKey);
            RSAPrivateKey priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded));
            //RSA解密
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, priKey);
            outStr = new String(cipher.doFinal(inputByte));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("RSA 解密错误");
        }
        return outStr;
    }

    public static void main(String[] args) throws Exception {
        String str = "ehdg3yAC3LMF2FWvhZfgi7/3zL2lGkt/uTkUSNIMqS85n2wUDjTEpXjTQ+IZfgyWxaxz7snXKVzcZ+IbrMR2hVn/yfoMPM+kcLfOZPS2wJhhrp65yPR4T5DqhBclNGAkkpagX5l3XSjCHyaQ4zKgWRiRuMtKNxWvcuPR1oI+/J4=";
        String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMSquRbRPoB6wjuVXZn/hZOMgFvU1rItbBb3AYCOI3sIuPjBRsb1xvRKcMoWqojWb+EAADT+9AF1ge7NoPLTm7cCAwEAAQ==";
        String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALYjUHtYhXGYPi1/\n" +
                "KAbQ+8UK32fuj1C/lL080aU2qWz1xEaXGuSpJ0pumPYd9kI9UCmNNsxXIW0MvdAZ\n" +
                "phE8jj1zUqgVADOv43ibbTcTf489qNRj+vNZO1S9PQTPX4+aN8XIpf185fdRE6eS\n" +
                "49XE5RW/61xtJn87jDvzcdGmoagNAgMBAAECgYAV44CUkjrXP/iY9yMUTqEzOO9I\n" +
                "Sj8m8R8q7tgzBrKtyYthAF/XAw0ZwNjZGxixNTUhCTuAqyf1NwbIr4xpneFqBQ9u\n" +
                "x8beFt4W+sj8y70Vu+6XrnCcuqQ1/1Mqtl8fQqyU356FEcht76N7qe/BNv/LBhyf\n" +
                "7mGRJycM9WYQ/Oyw8QJBAOsOhkCrt7xFRfO5ox6L4rB5lGPMAybAgDTsY/UTaX9X\n" +
                "dqhvZS9Sm18wi3ZQSFxv2QuhEHDECrhnp9ryTbnxaZsCQQDGXcAVagGNB7XP4JOs\n" +
                "V5ud0N8bFkMr0YeUG8ajHIKeERRaOENDYr4aqnIitLvdLcsCT4+gSPy+ruk9ttMB\n" +
                "skN3AkAHrdDK+hElkGnJnY3rqIw+Uap8XCTaNj25/OMDFaBDrbrNo939Xsh+oppb\n" +
                "wiGrC2hezeM4m97xyUkdiP7rDSzzAkA10bfM148GKYJdnSvTxY02GzZ9DeQ5nkav\n" +
                "m32gQZr6sd2YwM0XGlJDv9zpp6mUz9jVjqqELWlNfv1PPiwvuKLvAkEAkKOCcdi3\n" +
                "kVhI2jk/9VuaUU2TYMO8Pqk1qqjzDednEALmpDddpN4rAHlBGREerKduyMGEsTXJ\n" +
                "an/52W9Nf2QWSA==";

//        String decrypt = decrypt(str, privateKey);
        String encrypt = encrypt("123456" , publicKey);
        System.out.println(encrypt);
//        System.out.println(decrypt);

    }

}
