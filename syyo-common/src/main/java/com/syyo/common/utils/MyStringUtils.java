package com.syyo.common.utils;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;

import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @Auther: wangzhong
 * @Date: 2019/10/16 09:43
 * @Description:
 */
public class MyStringUtils {

    private static final String UNKNOWN = "unknown";
    /**
     * 将字符串转成列表  "0,2,4" => [0,2,4]
     * @param str
     * @return
     */
    public static List<Integer> stringToList(String str){
        List<Integer> list = new ArrayList<>();
        if (!StringUtils.isEmpty(str)){
            String[] splits = str.split(",");
            for (String split : splits) {
                list.add(Integer.parseInt(split));
            }
        }
        return list;
    }


    /**
     * 将整型列表转换为字符串 [0,2,4] => "0,2,4"
     * @param list
     * @return
     */
    public static String getPids(List<Integer> list){
        String pids =  "";
        if (list != null && list.size()>0){
            for (Integer integer : list) {
                pids = pids + ","+ integer;
            }
            return pids.substring(1, pids.length());
        }
        return "0";
    }

    /**
     * 将整型列表转换为字符串 [0,2,4] => "0,2,4"
     * @param list
     * @return
     */
    public static Integer getRank(List<Integer> list){
        if (list != null && list.size()>0){
            return list.size()+1;
        }
        return 1;
    }

    /**
     * 获取列表的最后一位数
     * @param list
     * @return
     */
    public static Integer getPid(List<Integer> list){
        if (list != null && list.size()>0){
            return list.get(list.size()-1);
        }
        return 0;

    }

    public static boolean isNotEmpty(String str) {
        return str != null && !"".equals(str);
    }

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static String getUUID(){
        return UUID.randomUUID().toString();
    }

    /**
     * 首字母小写
     * @param s
     * @return
     */
    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }

    /**
     * 将字符串的首字母转大写
     * @param str 需要转换的字符串
     * @return
     */
    public static String toUpperFirstOne(String str) {
        // 进行字母的ascii编码前移，效率要高于截取字符串进行转换的操作
        char[] cs=str.toCharArray();
        cs[0]-=32;
        return String.valueOf(cs);
    }

    /**
     * 将下划线换成小驼峰式
     * @param str 需要转换的字符串
     * @return
     */
    public static String toSmallHumpStr(String str) {
        String[] s = str.split("_");
        String humpStr = "";
        for (int i = 0; i <s.length; i++) {
            if (i ==0){
                humpStr = s[i];
            }else {
                humpStr = humpStr +  toUpperFirstOne(s[i]);
            }
        }
        return humpStr;
    }

    /**
     * 将下划线换成大驼峰式
     * @param str 需要转换的字符串
     * @return
     */
    public static String toBigHumpStr(String str) {
        String[] s = str.split("_");
        String humpStr = "";
        for (String s1 : s) {
            humpStr = humpStr +  toUpperFirstOne(s1);
        }
        return humpStr;
    }

    /**
     * 获取浏览器类型
     * @param request
     * @return
     */
    public static String getBrowser(HttpServletRequest request){
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        Browser browser = userAgent.getBrowser();
        return browser.getName();
    }


    /**
     * 获取ip地址
     */
    public static String getIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        String comma = ",";
        String localhost = "127.0.0.1";
        if (ip.contains(comma)) {
            ip = ip.split(",")[0];
        }
        if  (localhost.equals(ip))  {
            // 获取本机真正的ip地址
            try {
                ip = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        return ip;
    }
}
