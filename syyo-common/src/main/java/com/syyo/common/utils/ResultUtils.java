package com.syyo.common.utils;


import com.syyo.common.domain.ResultVo;
import com.syyo.common.enums.ResultEnum;

/**
 * @Auther: wangzhong
 * @Date: 2020/1/8 17:28
 * @Description: 封装的返回类
 */
public class ResultUtils {

    public static ResultVo ok(Object obj){
        ResultVo<Object> resultVo = new ResultVo<>();
        resultVo.setData(obj);
        resultVo.setCode(ResultEnum.OK.getCode());
        resultVo.setMessage(ResultEnum.OK.getMessage());
        return resultVo;
    }
}
