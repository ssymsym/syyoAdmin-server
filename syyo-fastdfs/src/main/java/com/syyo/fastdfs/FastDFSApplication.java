package com.syyo.fastdfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @Auther: wangzhong
 * @Date: 2019/12/3 11:47
 * @Description:
 */


@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class FastDFSApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastDFSApplication.class,args);

        System.out.println("       启动成功                 \n" +
                "    '----'        ||      //        \n" +
                "  //      \\\\      ||    //      \n" +
                " ||        ||     ||   //          \n" +
                " ||        ||     ||   \\\\           \n" +
                "  \\\\      //      ||    \\\\             \n" +
                "    '----'        ||      \\\\              ");

    }
}
