package com.syyo.fastdfs.controller;


import com.syyo.common.anno.AnonymousAccess;
import com.syyo.common.domain.FileUpload;
import com.syyo.common.domain.ResultVo;
import com.syyo.fastdfs.service.FastDFSService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/18 10:17
 * @Description:  fast文件上传
 */
@Api(tags = "系统：文件模块")
@RestController
@RequestMapping("FastDFS/file")
public class FastDFSController {

    @Autowired
    private FastDFSService fileService;

    /**
     * 上传
     * @param file
     */
    @AnonymousAccess
    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public ResultVo<FileUpload> uploadFast(MultipartFile file) {
        return fileService.uploadFast(file);
    }

    @AnonymousAccess
    @PostMapping("/upload2")
    public ResultVo<FileUpload> uploadFast2() {
        return fileService.uploadFast2();
    }

}
