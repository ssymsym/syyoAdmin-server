package com.syyo.fastdfs.service;

import com.syyo.common.domain.FileUpload;
import com.syyo.common.domain.ResultVo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/18 10:17
 * @Description:
 */
public interface FastDFSService {

    /**
     * 上传文件
     * @param file
     * @return
     */
    ResultVo<FileUpload> uploadFast(MultipartFile file);

    ResultVo<FileUpload> uploadFast2();
}
