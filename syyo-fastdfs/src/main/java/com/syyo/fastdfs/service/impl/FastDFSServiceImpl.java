package com.syyo.fastdfs.service.impl;

import com.luhuiguo.fastdfs.domain.StorePath;
import com.luhuiguo.fastdfs.service.FastFileStorageClient;
import com.syyo.common.domain.FileUpload;
import com.syyo.common.domain.ResultVo;
import com.syyo.common.utils.MyFileUtil;
import com.syyo.common.utils.ResultUtils;
import com.syyo.fastdfs.service.FastDFSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/18 10:17
 * @Description:
 */
@Service
public class FastDFSServiceImpl implements FastDFSService {

    @Autowired
    private FastFileStorageClient storageClient;

    @Override
    public ResultVo<FileUpload> uploadFast(MultipartFile myFile){
        FileUpload fileUpdate = new FileUpload();
        // myFile.getOriginalFilename():取到文件的名字
        // FilenameUtils.getExtension(""):取到一个文件的后缀名
        String filename = myFile.getOriginalFilename();
        String suffix = filename.substring(filename.lastIndexOf(".")+1);
        // 计算文件大小
        long fileS = myFile.getSize();
        String fileSizeString = MyFileUtil.FormetFileSize(fileS);
        // group1:指storage服务器的组名
        // myFile.getInputStream():指这个文件中的输入流
        // myFile.getSize():文件的大小
        // 这一行是通过storageClient将文件传到storage容器
        StorePath uploadFile = null;
        try {
            uploadFile = storageClient.uploadFile("group1", myFile.getInputStream(), fileS, suffix);
            String fullPath = uploadFile.getFullPath();
            String path = uploadFile.getPath();

            fileUpdate.setFileName(filename);
            fileUpdate.setUrl(fullPath);
            fileUpdate.setFileSize(fileSizeString);
            fileUpdate.setFileType(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 返回它在storage容器的的路径
        return ResultUtils.ok(fileUpdate);
    }

    @Override
    public ResultVo<FileUpload> uploadFast2() {
        System.out.println(222222222);

        return ResultUtils.ok(2);


    }
}
