package com.syyo.generator.controller;

import com.syyo.common.domain.ResultVo;
import com.syyo.generator.service.DatasourcesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/20 15:45
 * @Description: 代码生成器
 */
@Api(tags = "系统工具：表管理")
@RestController
@RequestMapping("/datasources")
public class DatasourcesController {

    @Autowired
    private DatasourcesService datasourcesService;

    /**
     * 查询当前库的所有表
     * @param
     * @return
     */
    @ApiOperation("查询当前库的所有表")
    @GetMapping("/table")
    public ResultVo getTable(){
        return datasourcesService.getTable();
    }

    /**
     * 查询当前表的所有字段
     * @param
     * @return
     */
    @ApiOperation("查询当前表的所有字段")
    @GetMapping("/column")
    public ResultVo getColumn(@RequestParam("tableName")String tableName){
        return datasourcesService.getColumn(tableName);
    }
}
