package com.syyo.generator.controller;

import com.syyo.common.domain.ResultVo;
import com.syyo.generator.domain.req.GeneratorReq;
import com.syyo.generator.service.GeneratorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
* @author wang
* @date 2020-08-20
*/
@Api(tags = "系统工具：代码生成器管理")
@RestController
@RequestMapping("/generator")
public class GeneratorController {

    @Autowired
    private GeneratorService generatorService;

     /**
     * 代码生成
     * @param
     * @return
     */
    @ApiOperation("代码生成")
    @PostMapping("/create/{tableName}")
    public ResultVo generator(@PathVariable String tableName){
        return (generatorService.generator(tableName));
    }

     /**
     * 代码预览
     * @param
     * @return
     */
    @ApiOperation("代码预览")
    @GetMapping("/preview/{tableName}")
    public ResultVo preview(@PathVariable String tableName){
        return generatorService.preview(tableName);
    }

    /**
     * 代码下载
     * @param
     * @return
     */
    @ApiOperation("代码下载")
    @PostMapping("/download/{tableName}")
    public ResultVo download(@PathVariable String tableName, HttpServletRequest request, HttpServletResponse response){
         return generatorService.download(tableName,request,response);
    }

    /**
     * 添加或修改代码生成器表配置
     * @param
     * @return
     */
    @ApiOperation("代码生成器表配置")
    @PostMapping("/config")
    public ResultVo addConfig(@RequestBody GeneratorReq req){
        return generatorService.addConfig(req);
    }

    /**
     * 生成器表配置详情
     * @param
     * @return
     */
    @ApiOperation("生成器表配置详情")
    @GetMapping("/config/{tableName}")
    public ResultVo getConfig(@PathVariable String tableName){
        return generatorService.getConfig(tableName);
    }

    /**
     * 生成器表配置详情列表
     * @param
     * @return
     */
    @ApiOperation("生成器表配置详情列表")
    @GetMapping("/list/{pageNum}/{pageSize}")
    public ResultVo findAll(@PathVariable("pageNum") Integer pageNum,
                            @PathVariable("pageSize") Integer pageSize,
                            GeneratorReq req){
        return generatorService.findAll(pageNum,pageSize,req);
    }

}