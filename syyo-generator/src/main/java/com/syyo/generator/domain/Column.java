package com.syyo.generator.domain;

import lombok.Data;

/**
 * <p>
 * 数据库表对象
 * </p>
 *
 * @author wang
 * @since 2020-08-20
 */
@Data
public class Column {

    private static final long serialVersionUID = 1L;

    /**
     * 库名
     */
    private String databaseName;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 字段名
     */
    private String columnName;

    /**
     * 字段类型
     */
    private String columnType;

    /**
     * 字段注释
     */
    private String columnComment;


}
