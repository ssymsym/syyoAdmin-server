package com.syyo.generator.domain;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 数据库对象
 * </p>
 *
 * @author wang
 * @since 2020-08-20
 */
@Data
public class Datasources  {

    private static final long serialVersionUID = 1L;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表注释
     */
    private String tableComment;

    /**
     * 表排序
     */
    private String tableCollation;

    /**
     * 引擎类型（InnoDB）
     */
    private String engine;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;


}
