package com.syyo.generator.domain;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/20 14:19
 * @Description:
 */
public class JavaProperties {
    // 表名
    private String tableName;
    // 作者
    private String author;
    // 时间
    private String date;
    // 包名
    private String pack;
    // 模块全名 syyo-common
    private String moduleFull;
    // 模块尾  common
    private String module;
    // 类名
    private String className;
    // 首字母小写类名
    private String lowerClassName;
    // 接口名称
    private String apiName;

    // 属性集合  需要改写 equals hash 保证名字可不重复 类型可重复
    private Set<Field> fields = new LinkedHashSet<>();
    // 导入类的不重复集合
//    private Set<String> imports = new LinkedHashSet<>();


    public JavaProperties(String tableName,String pack,String moduleFull,String module,String className,String lowerClassName, String author,String date,String apiName) {
        this.tableName = tableName;
        this.pack = pack;
        this.moduleFull = moduleFull;
        this.module = module;
        this.className = className;
        this.lowerClassName = lowerClassName;
        this.author = author;
        this.date = date;
        this.apiName = apiName;
    }

    public void addField(String fieldType, String fieldName,String fieldNameDb,String description,Boolean primaryKey) {
        // 处理 java.lang
//        final String pattern = "java.lang";
//        String fieldType = type.getName();
//        if (!fieldType.startsWith(pattern)) {
//            // 处理导包
//            imports.add(fieldType);
//        }
        Field field = new Field();
        // 处理成员属性的格式
//        int i = fieldType.lastIndexOf(".");
//        field.setFieldType(fieldType.substring(i + 1));
        field.setFieldType(fieldType);
        field.setFieldName(fieldName);
        field.setFieldNameDb(fieldNameDb);
        field.setDescription(description);
        field.setPrimaryKey(primaryKey);
        fields.add(field);
    }

    public String getTableName() {
        return tableName;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }

    public String getPack() {
        return pack;
    }

    public String getModule() {
        return module;
    }

    public String getModuleFull() {
        return moduleFull;
    }

    public String getClassName() {
        return className;
    }

    public String getLowerClassName() {
        return lowerClassName;
    }

    public String getApiName() {
        return apiName;
    }

    public Set<Field> getFields() {
        return fields;
    }

//    public Set<String> getImports() {
//        return imports;
//    }


    /**
     * 成员属性封装对象.
     */
    public static class Field {
        // 成员属性类型
        private String fieldType;
        // 成员属性名称
        private String fieldName;
        // 数据库字段名称
        private String fieldNameDb;
        // 是否是主键
        private Boolean primaryKey;
        // 注释说明
        private String description;

        public String getFieldType() {
            return fieldType;
        }

        public void setFieldType(String fieldType) {
            this.fieldType = fieldType;
        }

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }
        public String getFieldNameDb() {
            return fieldNameDb;
        }

        public void setFieldNameDb(String fieldNameDb) {
            this.fieldNameDb = fieldNameDb;
        }

        public Boolean getPrimaryKey() {
            return primaryKey;
        }

        public void setPrimaryKey(Boolean primaryKey) {
            this.primaryKey = primaryKey;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
        /**
         * 一个类的成员属性 一个名称只能出现一次
         * 我们可以通过覆写equals hash 方法 然后放入Set
         *
         * @param o 另一个成员属性
         * @return 比较结果
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Field field = (Field) o;
            return Objects.equals(fieldName, field.fieldName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fieldType, fieldName);
        }
    }
}
