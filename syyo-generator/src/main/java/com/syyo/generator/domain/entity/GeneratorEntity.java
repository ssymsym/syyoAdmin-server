package com.syyo.generator.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
* @author wang
* @date 2020-08-20
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "gen_config")
public class GeneratorEntity extends Model<GeneratorEntity>  {

    private static final long serialVersionUID = 1L;

    /** 用户名 */
    @TableId(value = "id", type = IdType.AUTO)
    private String  id;

    /** 表名 */
    private String  tableName;

    /** 作者 */
    private String  author;

    /** 模块名 */
    private String  moduleName;

    /** 包路径 */
    private String  pack;

    /** 是否覆盖 */
    private Boolean  cover;

    /** 前端生成路劲 */
    private String  apiPath;

    /** 接口名称 */
    private String  apiName;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}