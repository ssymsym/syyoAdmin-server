package com.syyo.generator.domain.req;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* @author wang
* @date 2020-08-20
*/
@Data
public class GeneratorReq implements Serializable {

    private String  id;

    /** 表名 */
    @NotEmpty(message = "表名不能为空")
    private String  tableName;

    /** 作者 */
    @NotEmpty(message = "作者不能为空")
    private String  author;

    /** 模块名 */
    @NotEmpty(message = "模块不名不能为空")
    private String  moduleName;

    /** 包路径 */
    @NotEmpty(message = "包名不能为空")
    private String  pack;

    /** 是否覆盖 */
    @NotNull(message = "是否覆盖不能为空")
    private Boolean  cover;

    /** 前端生成路劲 */
    @NotEmpty(message = "前端生成路劲不能为空")
    private String  apiPath;

    /** 接口名称 */
    @NotEmpty(message = "接口名称不能为空")
    private String  apiName;

}