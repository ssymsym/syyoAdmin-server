package com.syyo.generator.mapper;

import com.syyo.generator.domain.Column;
import com.syyo.generator.domain.Datasources;

import java.util.List;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/20 15:18
 * @Description:
 */
public interface DatasourcesMapper {

    /**
     * 查询当前数据库的所有表
     */
    List<Datasources> findDatasources();

    /**
     * 查询当前数据库的表的所有字段
     */
    List<Column> findColumn(String tableName);

    /**
     * 查询当前库下的表的主键
     */
    List<Column> findPrimaryKey(String tableName);
}
