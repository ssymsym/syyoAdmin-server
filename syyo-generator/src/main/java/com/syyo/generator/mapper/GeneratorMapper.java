package com.syyo.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.syyo.generator.domain.entity.GeneratorEntity;

/**
* @author wang
* @date 2020-08-20
*/
public interface GeneratorMapper extends BaseMapper<GeneratorEntity>{


}