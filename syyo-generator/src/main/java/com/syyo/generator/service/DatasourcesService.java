package com.syyo.generator.service;

import com.syyo.common.domain.ResultVo;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/20 15:45
 * @Description:
 */
public interface DatasourcesService {

    ResultVo getTable();

    ResultVo getColumn(String tableName);
}
