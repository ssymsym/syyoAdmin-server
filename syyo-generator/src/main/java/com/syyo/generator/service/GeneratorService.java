package com.syyo.generator.service;

import com.syyo.generator.domain.entity.GeneratorEntity;
import com.syyo.generator.domain.req.GeneratorReq;
import com.syyo.common.domain.ResultVo;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
* @author wang
* @date 2020-08-20
*/
public interface GeneratorService extends IService<GeneratorEntity>{

    /**
    * 新增
    * @return ResultVo
    */
    ResultVo generator(String tableName);

    /**
    * 删除
    * @return ResultVo
    */
    ResultVo preview(String tableName);

    /**
    * 编辑
    * @return ResultVo
    */
    ResultVo download(String tableName, HttpServletRequest request, HttpServletResponse response);

    /**
    * 添加或修改代码生成器表配置
    * @return ResultVo
    */
    ResultVo addConfig(GeneratorReq tableName);

    /**
    * 列表
    * @return ResultVo
    */
    ResultVo findAll(Integer pageNum, Integer pageSize, GeneratorReq req);

    /**
     * 查询代码生成器表配置
     * @return ResultVo
     */
    ResultVo getConfig(String tableName);
}