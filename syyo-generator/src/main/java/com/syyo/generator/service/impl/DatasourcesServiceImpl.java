package com.syyo.generator.service.impl;

import com.syyo.common.domain.ResultVo;
import com.syyo.common.utils.ResultUtils;
import com.syyo.generator.domain.Column;
import com.syyo.generator.domain.Datasources;
import com.syyo.generator.mapper.DatasourcesMapper;
import com.syyo.generator.service.DatasourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: wangzhong
 * @Date: 2020/8/20 15:45
 * @Description:
 */
@Service
public class DatasourcesServiceImpl implements DatasourcesService {

    @Autowired
    private DatasourcesMapper datasourcesMapper;


    @Override
    public ResultVo getTable() {
        List<Datasources> datasources = datasourcesMapper.findDatasources();
        ResultVo resultVo = new ResultVo();
        resultVo.setCode(200);
        resultVo.setMessage("ok");
        resultVo.setData(datasources);
        return ResultUtils.ok(datasources);


    }

    @Override
    public ResultVo getColumn(String tableName) {
        List<Column> columns = datasourcesMapper.findColumn(tableName);
        return ResultUtils.ok(columns);
    }
}
