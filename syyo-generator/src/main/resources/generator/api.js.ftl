import request from '@/utils/request'

export function add(data) {
  return request({
    url: '/${lowerClassName}',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/${lowerClassName}/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: '/${lowerClassName}',
    method: 'put',
    data
  })
}

export function get${className}(id) {
  return request({
  url: '/${lowerClassName}/' + id,
    method: 'get'
  })
}

export function list${className}(data) {
  return request({
    url: '/${lowerClassName}/list/' + data.pageNum + '/' + data.pageSize,
    method: 'get',
    data
  })
}

export default { add, edit, del, get${className}, list${className} }