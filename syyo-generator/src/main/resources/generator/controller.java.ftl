package ${pack}.${module}.controller;

import com.syyo.common.domain.ResultVo;
import com.syyo.common.anno.SysLog;
import ${pack}.${module}.domain.req.${className}Req;
import ${pack}.${module}.service.${className}Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
* @author ${author}
* @date ${date}
*/
@Api(tags = "系统：${apiName}管理")
@RestController
@RequestMapping("/${lowerClassName}")
public class ${className}Controller {

    @Autowired
    private ${className}Service ${lowerClassName}Service;

     /**
     * ${apiName}新增
     * @param
     * @return
     */
    @SysLog("${apiName}新增")
    @ApiOperation("${apiName}新增")
    @PreAuthorize("@syyo.check('system:${lowerClassName}:add')")
    @PostMapping
    public ResultVo add(@RequestBody ${className}Req req){
        return (${lowerClassName}Service.add(req));
    }

     /**
     * ${apiName}删除
     * @param
     * @return
     */
    @SysLog("${apiName}删除")
    @ApiOperation("${apiName}删除")
    @PreAuthorize("@syyo.check('system:${lowerClassName}:del')")
    @DeleteMapping("/{id}")
    public ResultVo del(@PathVariable("id") Integer id){
        return (${lowerClassName}Service.del(id));
    }

    /**
     * ${apiName}编辑
     * @param req
     * @return
     */
    @SysLog("${apiName}编辑")
    @ApiOperation("${apiName}编辑")
    @PreAuthorize("@syyo.check('system:${lowerClassName}:edit')")
    @PutMapping
    public ResultVo edit(@RequestBody ${className}Req req){
        return (${lowerClassName}Service.edit(req));
    }

    /**
     * ${apiName}详情
     * @param
     * @return
     */
    @ApiOperation("${apiName}详情")
    @GetMapping("/{id}")
    public ResultVo findOne(@PathVariable("id") Integer id){
        return (${lowerClassName}Service.findOne(id));
    }

    /**
     * ${apiName}列表
     * @param
     * @return
     */
    @ApiOperation("${apiName}列表")
    @GetMapping("/list/{pageNum}/{pageSize}")
    public ResultVo findAll(@PathVariable("pageNum") Integer pageNum,
                            @PathVariable("pageSize") Integer pageSize,
                            ${className}Req req){
        return ${lowerClassName}Service.findAll(pageNum,pageSize,req);
    }

}