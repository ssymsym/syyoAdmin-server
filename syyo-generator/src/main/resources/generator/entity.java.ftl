package ${pack}.${module}.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
<#list  fields as field>
    <#if field.fieldType=='LocalDateTime'>
import java.time.LocalDateTime;
    </#if>
    <#if field.fieldType=='LocalDate'>
import java.time.LocalDate;
    </#if>
    <#if field.fieldType=='Date'>
import java.util.Date;
    </#if>
    <#if field.fieldType=='Date'>
java.math.BigDecimal;
    </#if>
</#list>
import java.io.Serializable;

/**
* @author ${author}
* @date ${date}
* @Description: 实体类：和数据库对应的
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "${tableName}")
public class ${className}Entity extends Model<${className}Entity>  {

    private static final long serialVersionUID = 1L;

   <#list  fields as field>
        <#if field.primaryKey>
    /** ${field.description} */
    @TableId(value = "${field.fieldNameDb}", type = IdType.AUTO)
    private ${field.fieldType}  ${field.fieldName};
        </#if>

        <#if !field.primaryKey>
    /** ${field.description} */
    private ${field.fieldType}  ${field.fieldName};
        </#if>
   </#list>

       <#list  fields as field>
           <#if field.primaryKey>
    @Override
    protected Serializable pkVal() {
        return this.${field.fieldName};
    }
           </#if>
       </#list>

}