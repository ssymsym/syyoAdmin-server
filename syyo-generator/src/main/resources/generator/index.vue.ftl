<template>
  <div>
    <!--卡片-->
    <el-card class="box-card">
      <!-- 搜索与添加区域 -->
      <el-button v-hasPermi="['system:${lowerClassName}:add']" type="primary" small class="addBtn" style="margin:10px" size="mini" icon="el-icon-plus" @click="addDialogVisible = true">新增
      </el-button>
      <!--表格-->
      <el-table :data="listForm" stripe border style="width: 100%">
        <#list  fields as field>
        <el-table-column prop="${field.fieldName}" label="${field.description}" />
        </#list>
        <!--操作-->
        <el-table-column label="操作">
          <template slot-scope="scope">
            <el-tooltip :enterable="false" effect="dark" content="编辑" placement="top">
              <el-button v-hasPermi="['system:${lowerClassName}:edit']" type="success" icon="el-icon-edit" size="mini" @click="editBtn(scope.row.<#list fields as field><#if field.primaryKey>${field.fieldName}</#if></#list>)" />
            </el-tooltip>
            <el-tooltip :enterable="false" effect="dark" content="删除" placement="top">
              <el-button v-hasPermi="['system:${lowerClassName}:del']" type="danger" icon="el-icon-delete" size="mini" @click="delBtn(scope.row.<#list fields as field><#if field.primaryKey>${field.fieldName}</#if></#list>)" />
            </el-tooltip>
          </template>
        </el-table-column>
      </el-table>

      <!--分页区域-->
      <el-pagination
              background
              :current-page="pageNumber"
              :page-sizes="[1, 2, 5, 10]"
              :page-size="pageSize"
              layout="total, sizes, prev, pager, next, jumper"
              :total="total"
              @size-change="handleSizeChange"
              @current-change="handleCurrentChange"
      />

    </el-card>

    <!--添加对话框-->
    <el-dialog title="添加" :visible.sync="addDialogVisible" width="580px" @close="addHandleClose">
      <span>
        <el-form ref="addFormRef" :model="addForm" :rules="addFormRules" label-width="100px">
          <#list  fields as field>
            <el-form-item label="${field.description}" prop="${field.fieldName}">
            <el-input v-model="addForm.${field.fieldName}" />
          </el-form-item>
          </#list>
        </el-form>
        <!--底部区域-->
      </span>
      <span slot="footer" class="dialog-footer">
        <el-button @click="addDialogVisible = false">取 消</el-button>
        <el-button type="primary" @click="addClick">确 定</el-button>
      </span>
    </el-dialog>

    <!--编辑对话框-->
    <el-dialog title="编辑" :visible.sync="editDialogVisible" width="580px" @close="editHandleClose">
      <span>
        <el-form ref="editFormRef" :model="editForm" label-width="100px">
         <#list  fields as field>
           <el-form-item label="${field.description}" prop="${field.fieldName}">
            <el-input v-model="editForm.${field.fieldName}" />
          </el-form-item>
         </#list>
        </el-form>
        <!--底部区域-->
      </span>
      <span slot="footer" class="dialog-footer">
        <el-button @click="editDialogVisible = false">取 消</el-button>
        <el-button type="primary" @click="editClick">确 定</el-button>
      </span>
    </el-dialog>

  </div>
</template>

<script>
  import { add, edit, del, get${className}, list${className} } from '@/api/${module}/${lowerClassName}'

  export default {
    name: '${className}',
    data() {
      return {
        addDialogVisible: false,
        editDialogVisible: false,
        allotDialogVisible: false,
        pageNumber: 1,
        pageSize: 10,
        total: 1,
        listForm: [],
        addForm: {
          <#list  fields as field>
          ${field.fieldName}: '',
          </#list>
        },
        editForm: {},
        addFormRules: {
          <#list  fields as field>
          ${field.fieldName}: [
            { required: true, message: '请输入${field.description}', trigger: 'blur' },
            {
              min: 3,
              max: 10,
              message: '${field.description}的长度在3~10个字符之间',
              trigger: 'blur'
            }
          ],
          </#list>
        }
      }
    },
    created() {
      // 初始化列表
      this.initList${className}()
    },

    methods: {
      // 添加按钮
      addBtn() {
        this.addDialogVisible = true
      },

      // 确认添加
      addClick() {
        this.$refs.addFormRef.validate(valid => {
          if (!valid) return
          add(this.addForm).then(res => {
            if (res.code === 20000) {
              this.$message.success(res.message)
              // 刷新数据
              this.initList${className}()
            }
            // 关闭对话框
            this.addDialogVisible = false
          })
        })
      },

      // 编辑按钮
      editBtn(id) {
        this.editDialogVisible = true
        get${className}(id).then(res => {
          if (res.code === 20000) {
            this.editForm = res.data
          }
        })
      },

      // 确认编辑
      editClick() {
        this.$refs.editFormRef.validate(valid => {
          if (!valid) return
          edit(this.editForm).then(res => {
            if (res.code === 20000) {
              this.$message.success(res.message)
              // 刷新数据
              this.initList${className}()
            }
            // 关闭对话框
            this.editDialogVisible = false
          })
        })
      },


      // 删除按钮
      delBtn(id) {
        this.$confirm('此操作将永久删除该用户, 是否继续?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          // 确认操作
          del(id).then(res => {
            if (res.code === 20000) {
              this.$message.success(res.message)
              // 刷新数据
              this.initList${className}()
            }
          })
        })
      },

      // 监听当前页的事件
      handleCurrentChange(pageIndex) {
        const params = {
          pageNum: pageIndex,
          pageSize: this.pageSize
        }
        list${className}(params).then(res => {
          this.listForm = res.data.records
          this.total = res.data.total
          this.pageNum = res.data.current
          this.pageSize = res.data.size
        })
      },

      // 监听当前页的条数事件
      handleSizeChange(pageIndex) {
        const params = {
          pageNum: this.pageNumber,
          pageSize: pageIndex
        }
        list${className}(params).then(res => {
          this.listForm = res.data.records
          this.total = res.data.total
          this.pageNum = res.data.current
          this.pageSize = res.data.size
        })
      },

      // 监听添加对话框的关闭事件，重置表单内容
      addHandleClose() {
        this.$refs.addFormRef.resetFields()
      },

      // 监听编辑对话框的关闭事件,重置表单内容
      editHandleClose() {
        this.$refs.editFormRef.resetFields()
      },

      // 初始化数据
      initList${className}() {
        const params = {
          pageNum: this.pageNumber,
          pageSize: this.pageSize
        }
        list${className}(params).then(res => {
          this.listForm = res.data.records
          this.total = res.data.total
          this.pageNum = res.data.current
        })
      }
    }
  }
</script>

<style scoped>
</style>
