package ${pack}.${module}.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ${pack}.${module}.domain.entity.${className}Entity;

/**
* @author ${author}
* @date ${date}
*/
public interface ${className}Mapper extends BaseMapper<${className}Entity>{


}