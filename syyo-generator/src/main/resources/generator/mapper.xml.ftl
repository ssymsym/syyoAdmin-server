<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${pack}.${module}.mapper.${className}Mapper">


    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${pack}.${module}.domain.entity.${className}Entity">
<#list fields as field>
<#if field.primaryKey>
        <id property="${field.fieldName}" column="${field.fieldNameDb}" />
</#if>
    <#if !field.primaryKey>
        <result property="${field.fieldName}" column="${field.fieldNameDb}" />
    </#if>
</#list>
    </resultMap>

</mapper>
