package ${pack}.${module}.domain.req;

import lombok.Data;
<#list  fields as field>
    <#if field.fieldType=='LocalDateTime'>
import java.time.LocalDateTime;
    </#if>
    <#if field.fieldType=='LocalDate'>
import java.time.LocalDate;
    </#if>
    <#if field.fieldType=='Date'>
import java.util.Date;
    </#if>
</#list>
import java.io.Serializable;

/**
* @author ${author}
* @date ${date}
* @Description: 请求参数类
*/
@Data
public class ${className}Req implements Serializable {

        <#list  fields as field>
    /** ${field.description} */
    private ${field.fieldType}  ${field.fieldName};

        </#list>

}