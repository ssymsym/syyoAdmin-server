package ${pack}.${module}.service;

import ${pack}.${module}.domain.entity.${className}Entity;
import ${pack}.${module}.domain.req.${className}Req;
import com.syyo.common.domain.ResultVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ${author}
* @date ${date}
*/
public interface ${className}Service extends IService<${className}Entity>{

    /**
    * 新增
    * @return ResultVo
    */
    ResultVo add(${className}Req req);

    /**
    * 删除
    * @return ResultVo
    */
    ResultVo del(Integer id);

    /**
    * 编辑
    * @return ResultVo
    */
    ResultVo edit(${className}Req req);

    /**
    * 详情
    * @return ResultVo
    */
    ResultVo findOne(Integer id);

    /**
    * 列表
    * @return ResultVo
    */
    ResultVo findAll(Integer pageNum, Integer pageSize, ${className}Req req);

}