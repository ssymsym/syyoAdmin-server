package ${pack}.${module}.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.syyo.common.enums.ResultEnum;
import com.syyo.common.exception.SysException;
import com.syyo.common.domain.ResultVo;
import com.syyo.common.utils.ResultUtils;
import ${pack}.${module}.domain.req.${className}Req;
import ${pack}.${module}.domain.entity.${className}Entity;
import ${pack}.${module}.mapper.${className}Mapper;
import ${pack}.${module}.service.${className}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
* @author ${author}
* @date ${date}
*/
@Service
public class ${className}ServiceImpl extends ServiceImpl<${className}Mapper, ${className}Entity> implements ${className}Service {

    @Autowired
    private ${className}Mapper ${lowerClassName}Mapper;

    /**
    * 新增
    * @return ResultVo
    */
    @Override
    @Transactional
    public ResultVo add(${className}Req req){
        // todo 具体业务，封装实体类对象
        ${className}Entity ${lowerClassName} = new ${className}Entity();
        // 添加创建时间和更新时间
        LocalDateTime now = LocalDateTime.now();
<#list  fields as field>
    <#if field.fieldNameDb == "create_time" >
        ${lowerClassName}.setCreateTime(now);
    </#if>
    <#if field.fieldNameDb == "update_time" >
        ${lowerClassName}.setCreateTime(now);
    </#if>
</#list>
        int insert = ${lowerClassName}Mapper.insert(${lowerClassName});
        if (insert != 1){
            throw new SysException(ResultEnum.E_10001.getCode(),ResultEnum.E_10001.getMessage());
        }
        return ResultUtils.ok(insert);
    }

    /**
    * 删除
    * @return ResultVo
    */
    @Override
    @Transactional
    public ResultVo del(Integer id){
         // todo 具体业务
        int insert = ${lowerClassName}Mapper.deleteById(id);
        if (insert != 1){
            throw new SysException(ResultEnum.E_10002.getCode(),ResultEnum.E_10002.getMessage());
        }
        return ResultUtils.ok(insert);
    }

    /**
    * 编辑
    * @return ResultVo
    */
    @Override
    @Transactional
    public ResultVo edit(${className}Req req){

        // todo 具体业务
        ${className}Entity ${lowerClassName} = new ${className}Entity();
        // 添加更新时间
<#list  fields as field>
    <#if field.fieldNameDb == "update_time" >
        LocalDateTime now = LocalDateTime.now();
        ${lowerClassName}.setCreateTime(now);
    </#if>
</#list>
        int insert = ${lowerClassName}Mapper.updateById(${lowerClassName});
        if (insert != 1){
            throw new SysException(ResultEnum.E_10003.getCode(),ResultEnum.E_10003.getMessage());
        }
        return ResultUtils.ok(insert);
    }

    /**
    * 详情
    * @return ResultVo
    */
    @Override
    public ResultVo findOne(Integer id){
        // todo 具体业务
        ${className}Entity ${lowerClassName} = ${lowerClassName}Mapper.selectById(id);
        return ResultUtils.ok(${lowerClassName});
    }

    /**
    * 列表
    * @return ResultVo
    */
    @Override
    public ResultVo findAll(Integer pageNum, Integer pageSize, ${className}Req req){

        Page<${className}Entity> teacherPage = new Page<${className}Entity>(pageNum,pageSize);
        QueryWrapper<${className}Entity> wrapper = new QueryWrapper<>();
        // todo 构建条件
        IPage<${className}Entity> list = ${lowerClassName}Mapper.selectPage(teacherPage, wrapper);
        return ResultUtils.ok(list);
    }

}