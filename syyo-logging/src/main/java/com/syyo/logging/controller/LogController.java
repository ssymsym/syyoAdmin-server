package com.syyo.logging.controller;

import com.syyo.common.domain.ResultVo;
import com.syyo.logging.domain.req.LogReq;
import com.syyo.logging.service.LogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
* @author wang
* @date 2020-09-02 16:38:49
*/
@Api(tags = "系统监控：日志管理")
@RestController
@RequestMapping("/log")
public class LogController {

    @Autowired
    private LogService logService;

    /**
     * 日志详情
     * @param
     * @return
     */
    @ApiOperation("日志详情")
    @GetMapping("/{id}")
    public ResultVo findOne(@PathVariable("id") Integer id){
        return (logService.findOne(id));
    }

    /**
     * 日志列表
     * @param
     * @return
     */
    @ApiOperation("日志列表")
    @GetMapping("/list/{pageNum}/{pageSize}")
    public ResultVo findAll(@PathVariable("pageNum") Integer pageNum,
                            @PathVariable("pageSize") Integer pageSize,
                            LogReq req){
        return logService.findAll(pageNum,pageSize,req);
    }

}