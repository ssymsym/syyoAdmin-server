package com.syyo.logging.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
* @author wang
* @date 2020-09-02 16:38:49
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "log")
public class LogEntity extends Model<LogEntity>  {

    private static final long serialVersionUID = 1L;

    /**  */
    @TableId(value = "id", type = IdType.AUTO)
    private Long  id;


    /** 描述 */
    private String  title;

    /** 操作人 */
    private String  userName;

    /** 方法 */
    private String  method;

    /** 操作内容 */
    private String  content;

    /** ip */
    private String  ip;

    /** 耗时 */
    private Long  elapsedTime;

    /** 浏览器 */
    private String  browser;

    /** 日志类型 */
    private String  logType;

    /** 错误信息 */
    private String  errorMsg;

    /** 操作日期 */
    private LocalDateTime  createTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}