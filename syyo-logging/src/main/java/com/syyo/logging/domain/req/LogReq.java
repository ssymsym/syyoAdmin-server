package com.syyo.logging.domain.req;

import lombok.Data;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
* @author wang
* @date 2020-09-02 16:38:49
*/
@Data
public class LogReq implements Serializable {

    /**  */
    private Long  id;

    /** 描述 */
    private String  title;

    /** 操作人 */
    private String  userName;

    /** 方法 */
    private String  method;

    /** 操作内容 */
    private String  content;

    /** ip */
    private String  ip;

    /** 操作日期 */
    private LocalDateTime  createTime;

    /** 耗时 */
    private Long  elapsedTime;

    /** 浏览器 */
    private String  browser;

    /** 日志类型 */
    private String  logType;

    /** 错误信息 */
    private String  errorMsg;
}