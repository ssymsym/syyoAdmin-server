package com.syyo.logging.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.syyo.logging.domain.entity.LogEntity;

/**
* @author wang
* @date 2020-09-02 16:38:49
*/
public interface LogMapper extends BaseMapper<LogEntity>{


}