package com.syyo.logging.service;

import com.syyo.logging.domain.entity.LogEntity;
import com.syyo.logging.domain.req.LogReq;
import com.syyo.common.domain.ResultVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author wang
* @date 2020-09-02 16:38:49
*/
public interface LogService extends IService<LogEntity>{

    /**
    * 新增
    * @return ResultVo
    */
    ResultVo add(LogEntity req);

    /**
    * 详情
    * @return ResultVo
    */
    ResultVo findOne(Integer id);

    /**
    * 列表
    * @return ResultVo
    */
    ResultVo findAll(Integer pageNum, Integer pageSize, LogReq req);

}