package com.syyo.logging.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.syyo.common.enums.ResultEnum;
import com.syyo.common.exception.SysException;
import com.syyo.common.domain.ResultVo;
import com.syyo.common.utils.MyStringUtils;
import com.syyo.common.utils.ResultUtils;
import com.syyo.logging.domain.req.LogReq;
import com.syyo.logging.domain.entity.LogEntity;
import com.syyo.logging.mapper.LogMapper;
import com.syyo.logging.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
* @author wang
* @date 2020-09-02 16:38:49
*/
@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, LogEntity> implements LogService {

    @Autowired
    private LogMapper logMapper;

    /**
    * 新增
    * @return ResultVo
    */
    @Override
    @Transactional
    public ResultVo add(LogEntity log){
        int insert = logMapper.insert(log);
        return ResultUtils.ok(insert);
    }

    /**
    * 详情
    * @return ResultVo
    */
    @Override
    public ResultVo findOne(Integer id){
        LogEntity log = logMapper.selectById(id);
        return ResultUtils.ok(log);
    }

    /**
    * 列表
    * @return ResultVo
    */
    @Override
    public ResultVo findAll(Integer pageNum, Integer pageSize, LogReq req){
        Page<LogEntity> teacherPage = new Page<LogEntity>(pageNum,pageSize);
        QueryWrapper<LogEntity> wrapper = new QueryWrapper<>();
        if (MyStringUtils.isNotEmpty(req.getLogType())){
            wrapper.eq("log_type",req.getLogType());
        }
        // 创建时间倒序
        wrapper.orderByDesc("create_time");
        IPage<LogEntity> list = logMapper.selectPage(teacherPage, wrapper);
        return ResultUtils.ok(list);
    }

}